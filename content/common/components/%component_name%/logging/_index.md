---
title: "Журналы"
description: "Описание журналов и статистики"
weight: 60
type: docs
---

Узел %ProjectName% ведет следующие журналы:

* xDR:
  * [%cdr_file_1%.cdr](./edr/%cdr_file_1%/) — журнал % Add your text here %;
  * [%cdr_file_N%.cdr](./edr/%cdr_file_N%/) — журнал % Add your text here %.
* log:
  * [%log_file_1%.log](./log/%log_file_1%/) — журнал % Add your text here %;
  * [%log_file_N%.log](./log/%log_file_N%/) — журнал % Add your text here %;
  * %log_file_basic_1%.log — журнал % Add your text here %;
  * %log_file_basic_N%.log — журнал % Add your text here %.

Узел %ProjectName% ведет следующие файлы со статистиками по метрикам:

* [%stat_file_1%](./stat/%stat_file_1%/) — статистическая информация по метрикам %ShortProjectName% для % Add your text here %;
* [%stat_file_N%](./stat/%stat_file_N%/) — статистическая информация по метрикам %ShortProjectName% для % Add your text here %.
