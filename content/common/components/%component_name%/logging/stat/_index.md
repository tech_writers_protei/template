---
title: "Статистика %ShortProjectName%"
description: "Описание формата файлов статистики %ShortProjectName%"
weight: 30
type: docs
---

В разделе приведено описание файлов статистики.
По умолчанию файлы хранятся в директории `%stat_dir%`.

### Имя файла ###

Файлу статистики присваивается имя следующего формата: `%file_name_format%`.

Ниже приведены параметры, которые могут использоваться в имени файла при его создании.

| Параметр  | Описание                | Тип      |
|-----------|-------------------------|----------|
| %param_1% | % Add your text here %. | %type_1% |
| %param_N% | % Add your text here %. | %type_1% |

#### Пример ####

```text
%node_name_%stat_name_%YYYY-%MM-%DDT%hh:%mm%sign%hh%mm_%granularity.csv
MSK-MME02_MME-S11-interface_2023-07-06T05:15+0300_15min.csv
```

### Содержимое файла ###

Файл статистики имеет следующую структуру:

```csv
<stat_field_1>,<stat_field_N>
```

| Параметр     | Описание               | Тип           |
|--------------|------------------------|---------------|
| stat_field_1 | % Add your text here % | %stat_type_1% |
| stat_field_N | % Add your text here % | %stat_type_1% |

#### Пример файла ####

```csv
%value_11%,%value_1N%
%value_N1%,%value_NN%
```