---
title: "%log_name%"
description: "Журнал %log_description%"
weight: 20
type: docs
draft: true
---

### Описание используемых полей ###

| N   | Поле      | Описание               | Тип      |
|-----|-----------|------------------------|----------|
| 1   | %field_1% | % Add your text here % | %type_1% |
| M   | %field_M% | % Add your text here % | %type_M% |

#### Пример

```log
%value_1%,%value_M%
```