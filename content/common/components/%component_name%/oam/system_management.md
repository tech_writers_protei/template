---
title: "Управление"
description: "Основные команды управления узлом %ProjectName%"
weight: 20
type: docs
---

### Директории ###

В %ProjectName% используются следующие директории:

(Optional)
* `/usr/protei/%ProjectDir%/archive/` — директория для архивных файлов журналов;
* `/usr/protei/backup/%ProjectDir%/` — директория для резервных копий с других узлов;
* `/usr/protei/%ProjectDir%/bin/` — директория для исполняемых файлов;
* `/usr/protei/%ProjectDir%/bin/utils/` — директория для запускаемых скриптов, реализующих основной функционал скриптовой оболочки;
* `/usr/protei/%ProjectDir%/cdr/` — директория для журналов CDR;
* `/usr/protei/%ProjectDir%/conf/` — директория для общих файлов настройки сервера Tomcat;
* `/usr/protei/%ProjectDir%/config/` — директория для конфигурационных файлов;
* `/usr/protei/%ProjectDir%/db/` — директория для конфигурации параметров базы данных;
* `/usr/protei/%ProjectDir%/history/` — директория для архивных лог–файлов;
* `/usr/protei/%ProjectDir%/jar/` — директория для архивированных файлов с классами Java;
* `/usr/protei/%ProjectDir%/lib/` — директория для сторонних подключаемых библиотек;
* `/usr/protei/%ProjectDir%/logs/` — директория для журналов;
* `/usr/protei/%ProjectDir%/metrics/` — директория для файлов метрик;
* `/usr/protei/%ProjectDir%/scripts/` — директория для хранения скриптов CLI;
* `/usr/protei/%ProjectDir%/stat/` — директория для файлов статистики;
* `/usr/protei/%ProjectDir%/temp/` — директория для временных файлов;
* `/usr/protei/%ProjectDir%/webapps/` — директория для файлов Web–интерфейса;

### Управляющие команды ###

* Чтобы запустить %ProjectName%, следует выполнить одну из команд:

  * команду `systemctl start` от лица суперпользователя:

  ```bash
  $ sudo systemctl start %ProjectService%
  ```

  * скрипт `start` в рабочей папке:
  
  ```bash
  $ /usr/protei/%ProjectDir%/start
  ```

* Чтобы остановить %ProjectName%, следует выполнить одну из команд:

  * команду `systemctl stop` от лица суперпользователя:

  ```bash
  $ sudo systemctl stop %ProjectService%
  ```

  * скрипт `stop` в рабочей папке:

  ```bash
  $ /usr/protei/%ProjectDir%/stop
  ```

* Чтобы проверить текущее состояние %ProjectName%, следует выполнить одну из команд:

  * команду `systemctl status` от лица суперпользователя:

  ```console
  $ sudo systemctl status %ProjectService%
  ● %ProjectService%.service - %ShortProjectName%
     Loaded: loaded (/usr/lib/systemd/system/%ProjectService%.service; enabled; vendor preset: disable>
    Drop-In: /etc/systemd/system/%ProjectService%.service.d
             └─override.conf
     Active: active (running) since Fri 2023-07-21 19:50:25 MSK; 2 days ago
    Process: 998814 ExecStopPost=/usr/protei/%ProjectDir%/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
    Process: 998729 ExecStopPost=/usr/protei/%ProjectDir%/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
    Process: 998683 ExecStop=/usr/protei/%ProjectDir%/bin/utils/stop_prog.sh (code=exited, status=0/SUCCESS)
    Process: 999135 ExecStart=/usr/protei/%ProjectDir%/bin/utils/start_prog.sh (code=exited, status=0/SUCCESS)
    Process: 999108 ExecStartPre=/usr/protei/%ProjectDir%/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
    Process: 999082 ExecStartPre=/usr/protei/%ProjectDir%/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
   Main PID: 999151 (%ProjectDir%)
      Tasks: 11 (limit: 35612)
     Memory: 489.7M
     CGroup: /system.slice/%ProjectService%.service
             └─999151 ./bin/%ProjectDir%
  ```

  * скрипт `status` в рабочей папке:

  ```bash
  $ /usr/protei/%ProjectDir%/status
  ```

* Чтобы проверить текущую версию %ProjectName%, следует запустить скрипт `version` в рабочей папке:

```bash
$ /usr/protei/%ProjectDir%/version
```

* Чтобы перезагрузить %ProjectName%, следует выполнить одну из команд:

  * команду `systemctl restart` от лица суперпользователя:

  ```console
  $ sudo systemctl restart %ProjectService%
  ● %ProjectService%.service - %ShortProjectName%
     Loaded: loaded (/usr/lib/systemd/system/%ProjectService%.service; enabled; vendor preset: disabled)
     Active: active (running) since Fri 2023-07-21 19:50:25 MSK; 2 days ago
    Process: 99426 ExecStopPost=/usr/protei/%ProjectDir%/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
    Process: 99344 ExecStopPost=/usr/protei/%ProjectDir%/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
    Process: 99300 ExecStop=/usr/protei/%ProjectDir%/bin/utils/stop_prog.sh (code=exited, status=0/SUCCESS)
    Process: 99508 ExecStart=/usr/protei/%ProjectDir%/bin/utils/start_prog.sh (code=exited, status=0/SUCCESS)
    Process: 99481 ExecStartPre=/usr/protei/%ProjectDir%/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
    Process: 99453 ExecStartPre=/usr/protei/%ProjectDir%/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
   Main PID: 99524 (%ProjectDir%)
      Tasks: 1 (limit: 35612)
     Memory: 2.5M
     CGroup: /system.slice/%ProjectService%.service
             └─99524 ./bin/%ProjectDir%
  ```

  * скрипт `restart` в рабочей папке:

  ```bash
  $ /usr/protei/%ProjectDir%/restart
  ```

* Чтобы перезагрузить конфигурационный файл `file.cfg`, следует запустить скрипт `reload` в рабочей папке:

```console
$ /usr/protei/%ProjectDir%/reload <file.cfg>
reload <file> config Ok
```

* Чтобы записать дамп ядра, следует запустить скрипт `core_dump` в рабочей папке:

```console
$ /usr/protei/%ProjectDir%/core_dump
Are you sure you want to continue? y
Core dump generated!
```

**Примечание.** Файл дампа хранится в директории `/var/lib/systemd/coredump`.