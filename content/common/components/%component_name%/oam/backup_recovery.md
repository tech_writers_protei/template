---
title: "Резервное копирование и аварийное восстановление"
description: "Процедура резервного копирования и восстановления программного обеспечения узла %ProjectName%"
weight: 60
type: docs
draft: true
---

### Резервное копирование ### 

(Optional) Процедура резервного копирования узла %ShortProjectName% осуществляется выполнением скрипта **%script_name%** по
расписанию с помощью демона планировщика **cron** / **systemd.timer**.

Резервному копированию подлежат:

* %files_1%;
* %files_N.

(Optional) %hash_algorithm% хэш-сумма конфигурационных файлов записывается в файл `%hash_file_name%` в директории `/usr/protei/%ProjectDir%/config`.

(Optional) Необходимость обновления резервных копий определяется автоматически с помощью сравнения хэш-суммы текущих конфигурационных файлов
с хэш-суммой конфигурационных файлов в последней резервной копии, `%hash_file_name%`.

(Optional) Резервная копия сохраняется на дублирующий узел %ShortProjectName% в директорию `%backup_dir%` в виде архива **.tar.gz**.

Имя архива имеет формат: `%archive_name%`.

### Аварийное восстановление ###

Для выполнения аварийного восстановления узла %ShortProjectName% осуществляются следующие действия:

1. % Add your text here .%
...
N. % Add your text here .%

В результате будут восстановлены:

* %files_1%;
* %files_N%.