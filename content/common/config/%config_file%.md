---
title: "%config_file%.cfg"
description: "Параметры %config%"
weight: 30
type: docs
---

В файле задаются настройки % Add your text here %.

(Optional) **Примечание.** Наличие файла обязательно.

(Optional) Файл имеет формат JSON/YAML.

(Optional) Команда для перегрузки — `reload %config_file%`.

(Optional) **Предупреждение.** При модификации файла новая конфигурация будет применена только после перезапуска приложения %ShortProjectName%.

### Используемые секции ###

* [\[%Section 1%\]](#%section_1%) - параметры % Add your text here %;
  * [\[%Subsection_1%\]](#%subsection_1%) - параметры % Add your text here %;
  * [\[%Subsection_N%\]](#%subsection_N%) - параметры % Add your text here %;
* [\[%section_M%\]](#%section_M%) - параметры % Add your text here %;

### Описание параметров ###

| Параметр                                      | Описание                                                                                                                                         | Тип            | O/M          | P/R          | Версия            |
|-----------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|----------------|--------------|--------------|-------------------|
| **<a name="%section_1%">\[Section 1\]</a>**   | % Add your text here %.                                                                                                                          | %type_sec_1%   | %om_sec_1%   | %pr_sec_1%   | %version_sec_1%   |
| <a name="%subsection_1%">\[Subsection 1\]</a> | % Add your text here %.                                                                                                                          | %type_sub_1%   | %om_sub_1%   | %pr_sub_1%   | %version_sub_1%   |
| %parameter_1%                                 | % Add your text here %.<br>`%option_1%` / `%option_N%`.<br>Диапазон: %min%-%max%. По умолчанию: %default%.                                       | %type_param_1% | %om_param_1% | %pr_param_1% | %version_param_1% |
| %parameter_N%                                 | % Add your text here %.<br>`%option_1%` — %description_1%;<br>`%option_N%` — %description_N%.<br>Диапазон: %min%-%max%. По умолчанию: %default%. | %type_param_1% | %om_param_1% | %pr_param_1% | %version_param_1% |

#### Пример ####

```
[%Section_1%]
%Subsection_1% = {
  %parameter_1% = %default%;
}
```