---
title: "%stat_name%"
description: "Статистика %stat_description%"
weight: 20
type: docs
---

Файл **%stat_file_name%** содержит статистическую информацию по метрикам %ProjectShortName% для % Add your text here %

### Описание параметров ###

| Tx/Rx | Метрика     | Описание               | Группа                           |
|-------|-------------|------------------------|----------------------------------|
| Tx    | %metrics_1% | % Add your text here % | %field_1%:Value; %field_N%:Value |
| Rx    | %metrics_2% | % Add your text here % | %field_1%:Value; %field_N%:Value |
| Tx/Rx | %metrics_3% | % Add your text here % |                                  |
|       | %metrics_4% | % Add your text here % |                                  |

### Группа (Optional)

| Название  | Описание               | Тип      |
|-----------|------------------------|----------|
| %field_1% | % Add your text here % | %type_1% |
| %field_N% | % Add your text here % | %type_N% |

#### Пример группы (Optional)

```
%field_1%:%value_1%;
%field_N%:%value_N%;
```

#### Пример файла ####

```csv
%value_11%,%value_1N%
%value_M1%,%value_MN%
```