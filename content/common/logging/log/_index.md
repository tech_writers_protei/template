---
title: "Лог-файлы"
description: "Описание журналов работы приложения"
weight: 20
type: docs
draft: true
---

В разделе приведено описание log-файлов.
По умолчанию журналы log хранятся в директории `/usr/protei/log/%ProjectDir%`.