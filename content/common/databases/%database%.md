---
title: "Таблица %database_table%"
description: "% Add your text here %"
weight: 30
type: docs
---

### Поля %database_table%

| Field     | Type     | Null     | Key           | Description, Default, Extra |
|-----------|----------|----------|---------------|-----------------------------|
| %field_1% | %type_1% | %yes_no% | %pri_uni_mul% | % Add your text here %      |
| %field_N% | %type_N% | %yes_no% | %pri_uni_mul% | % Add your text here %      |
