---
title: "Скрипты командной строки"
description: "Скрипты %language% для методов HTTP API"
weight: 40
type: docs
---

В разделе в качестве примеров обращения к API используются скрипты, которые находятся в директории
`/usr/protei/%ProjectDir%/scripts/` на серверах %ProjectShortName%.

Описание скриптов и примеры применения можно получить в стандартном выводе при запуске скрипта без указания входных параметров.

* [%cli_command_1%](./%cli_command_1%/) - % Add your text here %;
* [%cli_command_N%](./%cli_command_N%/) - % Add your text here %.