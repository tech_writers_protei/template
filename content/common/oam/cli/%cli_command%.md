---
title: "%cli_command%"
description: "% Add your text here %"
weight: 30
type: docs
---

### Запрос

```bash
$ ./%cli_command% [%request_field_1%=%value_1%] [%request_field_N%=%value_N%]
```

#### Поля запроса

| Поле              | Описание               | Тип      | O/M    |
|-------------------|------------------------|----------|--------|
| %request_field_1% | % Add your text here % | %type_1% | %om_1% |
| %request_field_N% | % Add your text here % | %type_N% | %om_N% |

#### Пример запроса

```bash
$ ./s1_peers plmn 99999
```

#### Ответ

```text
[
  {
    "PLMN": <plmn_id>,
    "TAC": <tac>,
    "id": <enodeb_id>,
    "ip": <enodeb_ip>,
    "name": <enodeb_name>,
    "state": <enodeb_state>
  }
]
```

#### Поля ответа

| Поле  | Описание                  | Тип    | O/M |
|-------|---------------------------|--------|-----|
| PLMN  | Идентификатор сети PLMN.  | string | M   |
| TAC   | Код области отслеживания. | int    | M   |
| id    | Идентификатор eNodeB.     | string | M   |
| ip    | IP-адрес eNodeB.          | string | M   |
| name  | Имя eNodeB.               | string | M   |
| state | Флаг активности eNodeB.   | bool   | M   |

#### Пример ответа

```json5
{
  "PLMN": "99999",
  "TAC": 1,
  "id": "00101-176849",
  "ip": "172.30.153.1",
  "name": "enb2b2d1",
  "state": true
}
```
