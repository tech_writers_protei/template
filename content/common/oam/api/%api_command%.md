---
title: "%api_command%"
description: "% Add your text here %"
weight: 30
type: docs
---

Команда **%api_command%** позволяет % Add your text here %

### Запрос ###

```http
HTTP %METHOD% <host>:<port>/%link%/%api_command_1%?%request_field_1%=%value_1%&%request_field_N%=%value_N%

{
  "%request_body%": {}
}
```

### Поля запроса query ###

| Поле              | Описание               | Тип      | O/M    |
|-------------------|------------------------|----------|--------|
| %request_field_1% | % Add your text here % | %type_1% | %om_1% |
| %request_field_N% | % Add your text here % | %type_N% | %om_N% |

### Поля тела запроса ###

| Поле                   | Описание               | Тип      | O/M    |
|------------------------|------------------------|----------|--------|
| %request_body_field_1% | % Add your text here % | %type_1% | %om_1% |
| %request_body_field_N% | % Add your text here % | %type_N% | %om_N% |

### Пример запроса ###

```curl
curl -X %METHOD% http://localhost:8080/%link%/%cli_command_1%?%request_field_1%=%value_1%&%request_field_N%=%value_N% \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' \
  -H '%Header%: %Value%' \
  -d '{
  "%request_body%": {}
}'
```

### Ответ ###

```
%Non-standard Header%: %Value%

{
  "%response_body%": {}
}
```

### Пример ответа ###

```
<Non-standard Header>: <Value>

{
  "<response_body>": {}
}
```

### Поля ответа ###

| Поле               | Описание               | Тип      | O/M    |
|--------------------|------------------------|----------|--------|
| %response_field_1% | % Add your text here % | %type_1% | %om_1% |
| %response_field_N% | % Add your text here % | %type_N% | %om_N% |
