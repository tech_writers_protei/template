---
title: "KPI"
description: "Описание показателей производительности"
weight: 70
type: docs
draft: true
---

Для узла %ProjectName% отслеживаются следующие ключевые показатели:

* %performance_indicator_1%

%performance_indicator_name_1% = % Add your text here %

* %performance_indicator_N%

%performance_indicator_name_N% = % Add your text here %