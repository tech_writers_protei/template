---
title: "Сценарии работы"
description: "Описание порядка взаимодействия узлов при выполнении основных процедур"
weight: 50
type: docs
draft: true
---

ifndef::imagesdir[:imagesdir: ../images]

= Сценарии работы

В разделе приведено описание следующих сценариев:

* <<%procedure_anchor_1%,%procedure_name_1%>>;
* <<%procedure_anchor_N%,%procedure_name_N%>>;

[[%procedure_anchor_1%]]
=== Процедура %procedure_1%

image::%procedure_image%.svg[%procedure_scheme%]

.Алгоритм

. % Add your text here %
. % Add your text here %