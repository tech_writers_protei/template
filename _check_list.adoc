= Чек-лист

////
Здесь приведены основные моменты, которые необходимо отслеживать перед публикацией проекта на официальный портал.
Если не выполнен хотя бы один из пунктов, то документацию нельзя считать законченной, пусть даже и временно.
В таком случае файлы в проекте должны быть приведены к требуемому виду и формату.

Чтобы отметить пункт выполненным, необходимо пробел между квадратными скобками заменить на `x`.

[ ] -> [x]
////

[%interactive]
* [ ] Заменены все заглушки, используемые в шаблоне:
[%interactive]
** [ ] *_%ProjectName%_*;
** [ ] *_%ShortProjectName%_*;
** [ ] *_%ProjectDir%_*;
** [ ] *_%ProjectService%_*;
** [ ] *_% Add your text here %_*;
** [ ] *_(Optional)_*;
** [ ] Прочие заглушки.
[%interactive]
* [ ] В каждом файле корректно задан frontmatter;
* [ ] Удалены атрибуты *_draft: true_* во всех отображаемых файлах;
* [ ] Скрыты незаполненные файлы или предназначенные для внутреннего использования:
** Файлы перемещены из директории *_content/common/_* в другую корневую папку, или
** Добавлены атрибуты *_draft: true_* во всех скрываемых файлах.
* [ ] Проверены и исправлены все ссылки;
* [ ] Обновлен файл *__PDF_%ProjectDir%.yml__*, если существует.