---
title: "PROTEI-MIB"
description: "Main MIB"
weight: 30
type: docs
---

```mib
PROTEI-MIB DEFINITIONS ::= BEGIN

IMPORTS
    DisplayString FROM SNMPv2-TC
    enterprises FROM RFC1155-SMI;

protei			OBJECT IDENTIFIER ::= { enterprises 20873 }
sg			OBJECT IDENTIFIER ::= { protei 2 }
stp			OBJECT IDENTIFIER ::= { protei 300 }
smsc			OBJECT IDENTIFIER ::= { protei 141 }
proxy			OBJECT IDENTIFIER ::= { protei 150 }
scl			OBJECT IDENTIFIER ::= { protei 104 }
serviceBuilder		OBJECT IDENTIFIER ::= { protei 5385 }
proteiCPE		OBJECT IDENTIFIER ::= { protei 102 }
capl			OBJECT IDENTIFIER ::= { protei 157 }
ss7			OBJECT IDENTIFIER ::= { protei 3 }
dbrm			OBJECT IDENTIFIER ::= { protei 50 }
ipsmgw			OBJECT IDENTIFIER ::= { protei 250 }
ussi			OBJECT IDENTIFIER ::= { protei 251 }
mmsc			OBJECT IDENTIFIER ::= { protei 200 }
smsfw			OBJECT IDENTIFIER ::= { protei 120 }

OStateValue ::= DisplayString


END
```