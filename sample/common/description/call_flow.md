---
title: "Сценарии работы"
description: "Описание порядка взаимодействия узлов при выполнении основных процедур"
weight: 50
type: docs
---

В разделе приведено описание следующих сценариев:

* [Процедура присоединения (LTE Attach)](#lte_attach);
* [Процедура разрыва соединения по инициативе абонента (LTE Detach)](#lte_detach);
* [Процедура обновления зоны отслеживания (Tracking Area Update (TAU))](#tau).

### Процедура отключения по инициативе абонента (LTE Detach) {#lte_detach}

&nbsp;

![Detach call flow](UE_DETACH_SGW.svg)

1. UE отправляет запрос NAS DETACH REQUEST узлу MME. Запрос содержит GUTI и причину отключения от сети в Detach type.
2. Узел MME отправляет узлу SGW запрос GTPv2-C: Delete Session Request, который отключает активные EPS bearer-службы узла SGW, обслуживающие UE. 
3. Узел SGW передает узлу PGW сообщение GTPv2-C: Delete Session Request. Узел PGW подтверждает получение ответом GTPv2-C: Delete Session Response.
4. Узел SGW подтверждает получение ответом GTPv2-C: Delete Session Request.
5. Если значение поля Detach type не "Switch off" (отсоединение произошло не по причине отключения устройства), узел MME отправляет сообщение NAS DETACH ACCEPT устройству UE.
6. Узел MME разрывает соединение сигнального трафика с UE, отправив eNodeB сообщение NAS RELEASE COMMAND с причиной `Cause = Detach`.
