---
title: "HTTP API"
description: "Описание методов HTTP API"
weight: 30
type: docs
---

## Общая информация HTTP API ##

Все запросы обрабатываются в синхронном режиме.

Обработка всех клиентских запросов и отправка ответов также осуществляется в синхронном режиме.

Запросы передаются по протоколу HTTP.

Приложение должно использовать один адрес URL для отправки запросов:

```curl
http://host:port/%link%/command?arg=value&...&arg=value
```

| Поле    | Описание                                 | Тип       | O/M |
|---------|------------------------------------------|-----------|-----|
| host    | IP-адрес или DNS-имя PROTEI MME API.     | ip/string | M   |
| port    | Номер порта для установления соединения. | int       | M   |
| command | Имя выполняемой команды.                 | string    | M   |
| arg     | Параметр команды.                        | string    | O   |
| value   | Значение параметра.                      | Any       | O   |

(Optional) Все запросы осуществляются с помощью метода **HTTP GET**.

## Ответ от PROTEI MME ##

Если ответ на запрос требует передачи объекта, система отправляет объект в формате JSON и **Content-Type: application/json**.

Если ответ на запрос не требует передачи объекта, система отправляет статус выполнения запроса.

```text
200 OK
```

**Примечание.** Именно такой ответ передает система, если не указано иное.

## Команды ##

* [clear_dns_cache](#clear_dns_cache) - очистить cache адресов DNS;
* [deact_bearer](#deact_bearer) - деактивировать bearer-службу;
* [detach](#detach) - удалить регистрацию абонента из сети;
* [detach_by_vlr](#detach_by_vlr) - удалить регистрации абонентов на определенном VLR и запретить новые;
* [disconnect_enb](#disconnect_enb) - разорвать соединение с eNodeB;
* [enable_vlr](#enable_vlr) - разрешить регистрации на определенном VLR;
* [get_db_status](#get_db_status) - получить информацию о состоянии базы данных;
* [get_gtp_peers](#get_gtp_peers) - получить информацию о состоянии GTP-соединений;
* [get_location](#get_location) - получить местоположение абонента;
* [get_metrics](#get_metrics) - получить текущие значения метрик;
* [get_profile](#get_profile) - получить информацию об абоненте;
* [get_s1_peers](#get_s1_peers) - получить информацию о состоянии базовых станций LTE из хранилища MME;
* [get_sgs_peers](#get_sgs_peers) - получить информацию о состоянии SGs-соединений;
* [reset_metrics](#reset_metrics) - сбросить текущие значения метрик;
* [ue_disable_trace](#ue_disable_trace) - деактивировать трассировку на eNodeB для абонента;
* [ue_enable_trace](#ue_enable_trace) - активировать трассировку на eNodeB для абонента.

### Удалить регистрации абонентов на определенном VLR и запретить новые, detach_by_vlr {#detach_by_vlr}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/detach_by_vlr?ip=<ip>
```

#### Поля запроса ####

| Поле                              | Описание           | Тип | O/M |
|-----------------------------------|--------------------|-----|-----|
| <a name="ip_detach_by_vlr">ip</a> | IP-адрес узла VLR. | ip  | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach_by_vlr?ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **[ip](#ip_detach_by_vlr)**;