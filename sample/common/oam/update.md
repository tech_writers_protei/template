---
title: "Обновление"
description: "Инструкция по обновлению узла Protei_MME"
weight: 40
type: docs
---

Исполняемый файл узла PROTEI MME хранится в директории `/usr/protei/%ProjectDir%/bin`.

Для установки новой версии программного обеспечения узла %ShortProjectName% необходимо выполнить следующие действия:

1. Разместить новый исполняемый файл `%new_executable%` в папку `/usr/protei/%ProjectDir%/bin/`.
2. Изменить символическую ссылку на %ProjectDir% для запуска приложения, чтобы она указывала на исполняемый бинарный файл с требуемой версией %ShortProjectName%.

```bash
$ cd /usr/protei/%ProjectDir%/bin/
$ ln -sf %new_executable% %ProjectDir%
```

3. Перезапустить приложение.

```bash
$ systemctl restart %ProjectService%
```

4. Проверить состояние приложения.

```bash
$ systemctl status %ProjectService%
```

При необходимости, возврат к предыдущей версии программного обеспечения может быть выполнен по той же процедуре.
