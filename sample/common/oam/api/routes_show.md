
Запрос `routes_show` позволяет получить полную таблицу маршрутизации.
Ответ предоставляется в JSON формате.
Использование в запросе параметра позволяет получить в ответе только нулевой сегмент (default) таблицы маршрутизации, а также таблицы одного или нескольких виртуальных маршрутов.

Для получения информации по нескольким маршрутам имена маршрутов следует передать отдельными параметроми, разделяя их сиволом "&".

### Запрос ###

```curl
GET http://<host>:<port>/routes_show[?vrf=vrf_name]
```

### Поля запроса ###

| Параметр | Описание                                    | Тип    | O/M |
|----------|---------------------------------------------|--------|-----|
| vrf      | Название таблицы виртуальной маршрутизации. | string | O   |


### Пример запроса ###

```curl
GET http://localhost:5050/routes_show?vrf=vrf
```

#### Поля ответа ####

| Столбец                                         | Описание                                                                                        | Тип        | O/M |
|-------------------------------------------------|-------------------------------------------------------------------------------------------------|------------|-----|
| **[**                                           | Массив сегментов таблицы маршрутизации.                                                         | \[object\] |     |
| &nbsp;&nbsp;**{**                               | Сегмент таблицы маршрутизации.                                                                  | object     |     |
| &nbsp;&nbsp;&nbsp;&nbsp;name                    | Название сегмента таблицы маршрутизации.                                                        | string     | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;IPv4_family             | Флаг поддержки адресации IPv4.                                                                  | bool       | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;IPv6_family             | Флаг поддержки адресации IPv6.                                                                  | bool       | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;**routes**              | Перечень маршрутов, входящих в таблицу маршрутизации.                                           | \[object\] |     |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**                   |                                                                                                 |            |     |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Destination | IP-адрес узла назначения.                                                                       | ip         | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gateway     | IP-адрес шлюза.                                                                                 | ip         | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Genmask     | Битовая маска.                                                                                  | ip         | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Iface       | Название интерфейса.                                                                            | string     | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Weight      | Приоритет маршрута. Диапазон: 1-10.<br>**Примечание.** Чем больше значение, тем ниже приоритет. | int        | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BFD session | Название связанной с маршрутом BFD-сессии.                                                      | string     | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VRF name    | Имя соответствующей таблицы виртуальной маршрутизации. См. [eaf.conf](../../../config/eaf/)     | string     | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**                   |                                                                                                 |            |     |
| &nbsp;&nbsp;**}**                               |                                                                                                 |            |     |
| **]**                                           |                                                                                                 |            |     |

#### Пример успешного ответа ####

```json5
[
  {
    "name": "default",
    "IPv4_family": "true",
    "IPv6_family": "false",
    "routes": [
      {
        "Destination": "10.50.50.0",
        "Gateway": "192.168.99.213",
        "Genmask": "255.255.255.252",
        "Iface": "s1u",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      },
      {
        "Destination": "100.0.1.140",
        "Gateway": "0.0.0.0",
        "Genmask": "255.255.255.252",
        "Iface": "vlan100",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      },
      {
        "Destination": "10.12.0.0",
        "Gateway": "192.168.99.213",
        "Genmask": "255.255.255.224",
        "Iface": "s1u",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      },
      {
        "Destination": "10.199.190.0",
        "Gateway": "192.168.99.213",
        "Genmask": "255.255.255.0",
        "Iface": "s1u",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      },
      {
        "Destination": "172.30.31.0",
        "Gateway": "192.168.0.254",
        "Genmask": "255.255.255.0",
        "Iface": "s5u",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      },
      {
        "Destination": "172.30.165.0",
        "Gateway": "192.168.0.254",
        "Genmask": "255.255.255.0",
        "Iface": "s1u",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      },
      {
        "Destination": "192.168.0.0",
        "Gateway": "0.0.0.0",
        "Genmask": "255.255.128.0",
        "Iface": "s1u",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      },
      {
        "Destination": "192.168.0.0",
        "Gateway": "0.0.0.0",
        "Genmask": "255.255.128.0",
        "Iface": "s5u",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      },
      {
        "Destination": "172.18.0.0",
        "Gateway": "192.168.0.254",
        "Genmask": "255.255.0.0",
        "Iface": "s1u",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      },
      {
        "Destination": "0.0.0.0",
        "Gateway": "192.168.0.254",
        "Genmask": "0.0.0.0",
        "Iface": "s1u",
        "Weight": 10,
        "BFD session": "",
        "VRF name": "default"
      }
    ]
  }
]
```

#### Пример запроса ####

```curl
http://host:port/%link%/%cli_command_1%?%field_1%=%value_1%&%field_N%=%value_N%
```

#### Поля ответа ####

| Поле      | Описание         | Тип      | O/M    |
|-----------|------------------|----------|--------|
| %field_1% | %description_1%. | %type_1% | %om_1% |
| %field_N% | %description_N%. | %type_N% | %om_N% |

#### Ответ ####

```
%system_response%
```

#### Пример ответа ####

```
%system_response%
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `%code_1% %message_1%` - %descrtiption_1%;
* `%code_N% %message_N%` - %descrtiption_N%.
