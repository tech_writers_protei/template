---
title: "HTTP API"
description: "Описание методов HTTP API"
weight: 30
type: docs
---

В разделе приведено описание методов взаимодействия с узлом PROTEI PGW с помощью HTTP REST API-запросов.

## Общая информация HTTP API ##

Все запросы обрабатываются в синхронном режиме.

(Optional) Обработка всех клиентских запросов и отправка ответов также осуществляется в синхронном режиме.

(Optional) Запросы передаются по протоколу HTTP.

Приложение должно использовать один адрес URL для отправки запросов:

```curl
http://host:port/link/command?arg=value&...&arg=value
```

| Поле    | Описание                                 | Тип       | O/M |
|---------|------------------------------------------|-----------|-----|
| host    | IP-адрес или DNS-имя PROTEI PGW API.     | ip/string | M   |
| port    | Номер порта для установления соединения. | int       | M   |
| command | Имя выполняемой команды.                 | string    | M   |
| arg     | Параметр команды.                        | string    | O   |
| value   | Значение параметра.                      | Any       | O   |

(Optional) Все запросы осуществляются с помощью метода **HTTP GET**.

## Ответ от PROTEI PGW ##

(Optional) Если ответ на запрос требует передачи объекта, система отправляет объект в формате JSON и **Content-Type: application/json**.

(Optional) Если ответ на запрос не требует передачи объекта, система отправляет статус выполнения запроса.

```text
200 OK
```

## Команды ##

* [add_to_trace](./add_to_trace/) — активировать запись пользовательского трафика в дамп по номеру IMSI или IP-адресу;
* [bfd_client_stat](./bfd_client_stat/) — получить статистику клиента BFD;
* [cntr](./cntr/) — получить все доступные статистики;
* [core_stat](./core_stat/) — получить статистику ядра узла;
* [dhcp_client_stat](./dhcp_client_stat/) — получить статистику клиентской стороны протокола DHCP;
* [diameter_client_stat](./diameter_client_stat/) — получить статистику клиентской стороны протокола Diameter;
* [drop_connection](./drop_connection/) — удалить активное подключение абонента по номеру IMSI или IP-адресу;
* [flush_dump](./flush_dump/) — записать дамп сетевого трафика;
* [get_apn_offload](./get_apn_offload/) — получить информацию о доступности APN, с которыми работает узел PROTEI PGW;
* [get_info](./get_info/) — получить информацию об абонентской сессии по номеру IMSI или MSISDN;
* [get_trace_list](./get_trace_list/) — получить список номеров IMSI и IP-адресов абонентов, для которых ведется запись пользовательского трафика в дамп;
* [gtpc_client_stat](./gtpc_client_stat/) — получить статистику клиентской стороны протокола GTP-C;
* [http2_client_stat](./http2_client_stat/) — получить статистику клиентской стороны протокола HTTP2;
* [http2_server_stat](./http2_server_stat/) — получить статистику серверной стороны протокола HTTP2;
* [master_uptime](./master_uptime/) — получить дату и время запуска PROTEI PGW;
* [net_stat](./net_stat/) — получить статистику сети;
* [oam_stat](./oam_stat/) — получить статистику OAM;
* [radius_client_stat](./radius_client_stat/) — получить статистику клиентской стороны протокола RADIUS;
* [recovery_counter](./recovery_counter/) — получить количество перезапусков узла PROTEI PGW с момента установки;
* [reload_core](./reload_core/) — применить изменения в конфигурационном файле [core.conf](../../config/core/);
* [reload_dscp_qos](./reload_dscp_qos/) — применить изменения в конфигурационном файле [dscp_qos.conf](../../config/dscp_qos/);
* [reload_ifaces](./reload_ifaces/) — применить изменения в секции [net.conf::ifaces](../../config/net/#ifaces/);
* [reload_net](./reload_net/) — применить изменения в конфигурационном файле [net.conf](../../config/net/);
* [reload_trace](./reload_trace/) — применить изменения в конфигурационном файле [trace.conf](../../config/trace/);
* [remove_from_trace](./remove_from_trace/) — отключить запись пользовательского трафика в дамп по номеру IMSI или IP-адресу;
* [set_apn_down](./set_apn_down/) — деактивировать APN;
* [set_apn_up](./set_apn_up/) — активировать APN;
* [show_applyed_config](./show_applyed_config/) — получить список текущих значений параметров конфигурации узла;
* [show_arp](./show_arp/) — получить таблицу соответствия IP-адресов и MAC-адресов интерфейсов;
* [show_routes](./show_routes/) — получить таблицу маршрутизации;
* [start_write_all_traffic](./start_write_all_traffic/) — активировать запись всего пользовательского трафика в дамп;
* [stop](./stop/) — остановить работу узла PROTEI PGW;
* [stop_write_all_traffic](./stop_write_all_traffic/) — отключить запись всего пользовательского трафика в дамп;
* [switch_role(master/slave)](./switch_role/) — изменить роль узла на Master или Slave (ведомый).