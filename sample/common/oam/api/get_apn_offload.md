---
title: "get_apn_offload"
description: "Получение информации о разгрузке APN"
weight: 30
type: docs
---

Запрос **get_apn_offload** позволяет получить информацию о доступности APN, с которыми работает узел PGW.
Ответ предоставляется в формате JSON.

Активация APN осуществляется запросом [set_apn_up](../set_apn_up/).

Деактивация APN осуществляется запросом [set_apn_down](../set_apn_down/).

### Запрос ###
```
GET http://<host>:<port>/get_apn_offload
```

### Поля запроса ###

Отсутствуют.

### Поля ответа ###

| Поле                    | Описание                                                                                                                                                                                                                                                                                                                | Тип    | O/M |
|-------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| cmd_set_up              | Признак активации/деактивации APN с помощью API-запроса. <br>Возможные значения:<br> `0` — APN был деактивирован с помощью запроса [set_apn_down](./set_apn_down/);<br> `1` — APN был активирован с помощью запроса [set_apn_up](./set_apn_up/);<br>`-1` — API-запрос не был использован при активации/деактивации APN. | int    | M   |
| gw_availability_enabled | Флаг активации функции проверки доступности шлюза APN.<br/>**Примечание.** Функция активируется параметром [core::apns::gw_availability](../../../config/core/#gw_availability).                                                                                                                                        | flag   | M   |
| gw_available            | Флаг доступности шлюза APN.                                                                                                                                                                                                                                                                                             | flag   | M   |
| name                    | Имя точки доступа.                                                                                                                                                                                                                                                                                                      | string | M   |
| offload                 | Флаг доступности APN.                                                                                                                                                                                                                                                                                                   | flag   | M   |


#### Пример ответа ####
```
{
	"apns": [
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "67",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "68",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "99",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 1,
			"gw_available": 0,
			"name": "avail",
			"offload": 1
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "default_non-ip",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "ims",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "ims-test",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "internet",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "iot2",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "ixia",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "mcptt",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "mcptt.rx",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "mcptt.test",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "nbiot",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "radius.test",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "stepanov",
			"offload": 0
		},
		{
			"cmd_set_up": -1,
			"gw_availability_enabled": 0,
			"gw_available": 1,
			"name": "stepanov-radius",
			"offload": 0
		}
	]
}
```