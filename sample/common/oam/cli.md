---
title: "Скрипты командной строки"
description: "Скрипты bash для методов HTTP API"
weight: 35
type: docs
---

В этом разделе в качестве примеров обращения к API используются bash-скрипты, которые находятся в директории
`/usr/protei/%ProjectDir%/scripts/` на серверах MME.

Описание скриптов и примеры применения можно получить в стандартном
выводе при запуске скрипта без указания входных параметров.

* [clear_dns_cache](#clear_dns_cache) — очистить cache адресов DNS;
* [deact_bearer](#deact_bearer) — деактивировать bearer-службу;
* [detach](#detach) — удалить регистрацию абонента из сети;
* [detach_by_vlr](#detach_by_vlr) — удалить регистрации абонентов на определенном VLR и запретить новые;
* [diam_state](#diam_state) — получить информацию о состоянии PCSM-узлов; 
* [enable_vlr](#enable_vlr) — разрешить регистрации на определенном VLR;
* [enb_disconnect](#enb_disconnect) — разорвать соединение с eNodeB;
* [get_bearers](#get_bearers) — получить информацию о bearer-службах;
* [get_location](#get_location) — получить местоположение абонента;
* [get_metrics](#get_metrics) — получить текущие значения метрик;
* [get_profile](#get_profile) — получить информацию об абоненте;
* [gtp_peers](#gtp_peers) — получить информацию о состоянию GTP-соединений;
* [s1_peers](#s1_peers) — получить информацию о состоянии базовых станций LTE из хранилища MME;
* [s1_table](#s1_table) — получить информацию о состоянии базовых станций LTE из хранилища MME в табличном виде;
* [sgs_peers](#sgs_peers) — получить информацию о состоянии SGs-соединений;
* [trace_UE](#trace_ue) — добавить/удалить трассировку на eNodeB для абонента.

### % Add your text here. %, %command_cli_1% {#%command_cli_1%}

#### Запрос

```bash
$ ./%command_cli_1%
```

#### Ответ

```console
<status>
```

#### Пример ответа

```console
Done
```

### Деактивировать bearer-службу, deact_bearer {#deact_bearer}

#### Запрос

```bash
$ ./deact_bearer {imsi} {bearer_id}
```

#### Поля запроса ####

| Поле      | Описание                                    | Тип    | O/M |
|-----------|---------------------------------------------|--------|-----|
| imsi      | Номер IMSI абонента.                        | string | M   |
| bearer_id | Идентификатор деактивируемой bearer-службы. | int    | M   |

#### Пример запроса

```bash
$ ./deact_bearer 001010000000390 5
```

#### Ответ

```console
<status>
```

#### Пример ответа

```console
Done
```

### Удалить регистрацию абонента из сети, detach {#detach}

**Примечание.** При выполнении команды со значениями по умолчанию:
`reattach = 0`, регистрация абонента удаляется без последующей перерегистрации;
`purge = 1`, направляется запрос в HSS для удаления данных о VLR в абонентском профиле.

При `reattach = 1` удаляется регистрация с последующей перерегистрацией;
при `purge = 0` запрос на HSS не направляется.

#### Запрос

```bash
$ ./detach {imsi} [reattach={reattach}] [purge={purge}]
```

#### Поля запроса ####

| Поле     | Описание                                                   | Тип    | O/M |
|----------|------------------------------------------------------------|--------|-----|
| imsi     | Номер IMSI абонента.                                       | string | M   |
| reattach | Флаг переподключения к сети.<br>По умолчанию: 0.           | bool   | O   |
| purge    | Флаг полного удаления профиля из сети.<br>По умолчанию: 1. | bool   | O   |

#### Пример запроса

```bash
$ ./detach 001010000000390 reattach=1
```
#### Ответ

```console
<status>
```

#### Пример ответа

```console
Done
```

### Удалить регистрации абонентов на определенном VLR и запретить новые, detach_by_vlr {#detach_by_vlr}

#### Запрос ####

```bash
$ ./detach_by_vlr <ip>
```

#### Поля запроса ####

| Поле | Описание           | Тип | O/M |
|------|--------------------|-----|-----|
| ip   | IP-адрес узла VLR. | ip  | M   |

#### Пример запроса ####

```bash
$ ./detach_by_vlr 192.168.1.1
```

#### Ответ ####

```console
<status>
```

#### Пример ответа ####

```console
Done
```

### Получить информацию о состоянии всех Diameter PCSM, diam_state {#diam_state}

#### Запрос

```bash
$ ./diam_state
```

#### Ответ

```console
———-DIAM PCSM TABLE———- +
NAME             STATE
pcsm_name   pcsm_state
```

#### Поля ответа

| Поле  | Описание                                  | Тип    | O/M |
|-------|-------------------------------------------|--------|-----|
| name  | Имя узла Diameter PCSM.                   | string | M   |
| state | Состояние узла.<br>`"ACTIVATE"`/`"FAIL"`. | string | M   |

#### Пример ответа

```console
———-DIAM PCSM TABLE———- +
NAME             STATE
"Sg.DIAM.PCSM.1" "ACTIVATE"
"Sg.DIAM.PCSM.2" "ACTIVATE"
"Sg.DIAM.PCSM.3" "FAIL"
"Sg.DIAM.PCSM.4" "FAIL"
"Sg.DIAM.PCSM.5" "FAIL"
```

### Разрешить регистрации на определенном VLR, enable_vlr {#enable_vlr}

#### Запрос ####

```bash
$ ./enable_vlr <ip>
```

#### Поля запроса ####

| Поле | Описание           | Тип | O/M |
|------|--------------------|-----|-----|
| ip   | IP-адрес узла VLR. | ip  | M   |

#### Пример запроса ####

```bash
$ ./detach_by_vlr 192.168.1.1
```

#### Ответ ####

```console
<status>
```

#### Пример ответа ####

```console
Done
```

### Разорвать соединение с eNodeB, enb_disconnect {#enb_disconnect}

#### Запрос

```bash
$ ./enb_disconnect {ip}
```

#### Поля запроса

| Поле | Описание         | Тип | O/M |
|------|------------------|-----|-----|
| ip   | IP-адрес eNodeB. | ip  | M   |

#### Пример запроса

```bash
$ ./enb_disconnect 192.168.1.1
```

#### Ответ ####

```console
<status>
```

#### Пример ответа ####

```console
Done
```

### Получить информацию о bearer-службах, get_bearers {#get_bearers}

#### Запрос

```bash
$ ./get_bearers
```

#### Ответ

```console
      IMSI       ;    MSISDN     ;       GUTI        ;     ENB_IP      ;   CellId   ; EMM  ;  ECM  ; Subscriber_IP    ;
-----------------------------------------------------------------------------------------------------------------------
     <imsi>      ;    <msisdn>   ;      <guti>       ;      <enb_ip>   ;  <cell_id> ; <emm> ; <ecm> ; <subscriber_ip> ;
```

#### Поля ответа

| Поле          | Описание                                                                        | Тип    | O/M |
|---------------|---------------------------------------------------------------------------------|--------|-----|
| IMSI          | Номер IMSI абонента.                                                            | string | M   |
| MSISDN        | Номер MSISDN абонента.                                                          | string | M   |
| GUTI          | Глобальный уникальный временный идентификатор абонента.                         | string | M   |
| ENB\_IP       | IP-адрес eNodeNB.                                                               | ip     | M   |
| CellId        | Идентификатор соты.                                                             | string | M   |
| EMM           | Состояние EMM для UE абонента.<br>`0` - EMM-DEREGISTERED;/`1` - EMM-REGISTERED. | int    | M   |
| ECM           | Состояние ECM для UE абонента.<br>`0` - ECM-IDLE;/`1` - ECM-CONNECTED.          | int    | M   |
| Subscriber_IP | IP-адрес абонента.                                                              | ip     | O   |

#### Пример ответа

```console
      IMSI       ;    MSISDN     ;       GUTI        ;     ENB_IP      ;   CellId   ; EMM  ; ECM  ; Subscriber_IP ;
-------------------------------------------------------------------------------------------------------------------
 001010000000398 ;  76000000398  ;   99999;1;1;339   ;  172.30.153.1   ;  45273345  ;  0   ;  0   ;
```

### Получить местоположение абонента, get_location {#get_location}

#### Запрос

```bash
$ ./get_location {imsi}
```

#### Поля запроса

| Поле | Описание             | Тип    | O/M |
|------|----------------------|--------|-----|
| imsi | Номер IMSI абонента. | string | M   |

#### Пример запроса

```bash
$ ./get_location 001010000000390
```

#### Ответ

```text
{
  "CellID": <cell_id>,
  "ENB-ID": <enodeb_id>,
  "EPS_State": <eps_state>,
  "PLMN": <plmn_id>,
  "TAC": <tac>
}
```

#### Поля ответа ####

| Поле                    | Описание                    | Тип    | O/M |
|-------------------------|-----------------------------|--------|-----|
| CellID                  | Идентификатор соты.         | hex    | M   |
| ENB-ID                  | Идентификатор eNodeB.       | string | M   |
| [EPS_State](#eps_state) | Код состояния абонента EPS. | int    | M   |
| PLMN                    | Идентификатор сети PLMN.    | string | M   |
| TAC                     | Код области отслеживания.   | int    | M   |


#### Значения EPS State {#eps_state}

Полное описание см. [3GPP TS 23.078](https://www.etsi.org/deliver/etsi_ts/123000_123099/123078/17.00.00_60/ts_123078v170000p.pdf).

* `0` - Detached;
* `1` - AttachedNotReachableForPaging;
* `2` - AttachedReachableForPaging;
* `3` - ConnectedNotReachableForPaging;
* `4` - ConnectedReachableForPaging;
* `5` - NotProvidedFromSGSN;

#### Пример ответа

```json5
{
  "CellID": 45273345,
  "ENB-ID": "00101-176849",
  "EPS_State": 4,
  "PLMN": "00101",
  "TAC": 1
}
```

### Получить текущие значения метрик, get_metrics {#get_metrics}

#### Запрос ####

```bash
$ ./get_metrics
```

#### Пример ответа ####

```console
Total records count: 5
Number of attached subscribers: 2
Number of default bearers: 0
Number of dedicated bearers: 0

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 10847  100 10847    0     0   271k      0 --:--:-- --:--:-- --:--:--  278k
{
  "Diameter": {
    "capabilitiesExchangeAnswer": 0,
    "capabilitiesExchangeAnswer2001": 0,
    "capabilitiesExchangeRequest": 0,
    "deviceWatchdogAnswer": 155,
    "deviceWatchdogAnswer2001": 155,
    "deviceWatchdogRequest": 26
  },
  "Handover": {
    "interNodeHandoverRequestFromLteToUmts": 0,
    "interNodeHandoverRequestFromUmtsToLte": 0,
    "interNodeHandoverSuccessFromLteToUmts": 0,
    "interNodeHandoverSuccessFromUmtsToLte": 0,
    "interS1BasedHandoverRequestSgwChange": 0,
    "interS1BasedHandoverRequestSgwNotChange": 0,
    "interS1BasedHandoverSuccessSgwChange": 0,
    "interS1BasedHandoverSuccessSgwNotChange": 0,
    "intraS1BasedHandoverRequestSgwChange": 0,
    "intraS1BasedHandoverRequestSgwNotChange": 0,
    "intraS1BasedHandoverSuccessSgwChange": 0,
    "intraS1BasedHandoverSuccessSgwNotChange": 0,
    "intraX2BasedHandoverRequestSgwChange": 0,
    "intraX2BasedHandoverRequestSgwNotChange": 0,
    "intraX2BasedHandoverSuccessSgwChange": 0,
    "intraX2BasedHandoverSuccessSgwNotChange": 0
  },
  "Paging": {
    "s1PagingRequest": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestCsfb": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestCsfbLastEnodeb": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestCsfbLastTa": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestLastEnodeb": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestLastTa": {
      "": {
        "Value": 0
      }
    },
    "s1PagingSuccess": {
      "": {
        "Value": 0
      }
    },
    "s1PagingSuccessCsfb": {
      "": {
        "Value": 0
      }
    }
  },
  "Resource": {
    "averageCpuUtilization": 1.0854070276353633,
    "maxCpuUtilization": 1.402805611222445
  },
  "S1-Security": {
    "authenticationRequest": 4,
    "authenticationSuccess": 2,
    "securityModeCommand": 4,
    "securityModeComplete": 4
  },
  "S1-Service": {
    "ExtendedServiceRequest": {
      "": {
        "Value": 0
      }
    },
    "ExtendedServiceSuccess": {
      "": {
        "Value": 0
      }
    },
    "ServiceRequest": {
      "": {
        "Value": 0
      }
    },
    "ServiceSuccess": {
      "": {
        "Value": 0
      }
    },
    "csfbMoInitialContextSetupResponse": {
      "": {
        "Value": 0
      }
    },
    "csfbMoUeContextModificationResponse": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-attach": {
    "attachRequest": {
      "": {
        "Value": 4
      },
      "IMSIPLMN:20893": {
        "Value": 4
      },
      "TAI:208930001": {
        "Value": 4
      }
    },
    "attachSuccess": {
      "": {
        "Value": 4
      },
      "IMSIPLMN:20893": {
        "Value": 4
      },
      "TAI:208930001": {
        "Value": 4
      }
    },
    "combinedAttachRequest": {
      "": {
        "Value": 0
      }
    },
    "combinedAttachSuccess": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-bearer-activation": {
    "dedicatedBearerActiveRequest": {
      "": {
        "Value": 0
      }
    },
    "dedicatedBearerActiveSuccess": {
      "": {
        "Value": 0
      }
    },
    "pdnConnectivityRequest": {
      "": {
        "Value": 4
      },
      "IMSIPLMN:20893": {
        "Value": 4
      },
      "TAI:208930001": {
        "Value": 4
      }
    }
  },
  "S1-bearer-deactivation": {
    "deactivateEpsBearerContextAccept": {
      "": {
        "Value": 0
      }
    },
    "deactivateEpsBearerContextRequest": {
      "": {
        "Value": 0
      }
    },
    "dedicatedBearerDeactivationRequest": {
      "": {
        "Value": 0
      }
    },
    "defaultBearerDeactivationRequest": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-bearer-modification": {
    "hssInitBearerModRequest": {
      "": {
        "Value": 0
      }
    },
    "hssInitBearerModSuccess": {
      "": {
        "Value": 0
      }
    },
    "pgwInitBearerModRequest": {
      "": {
        "Value": 0
      }
    },
    "pgwInitBearerModSuccess": {
      "": {
        "Value": 0
      }
    },
    "ueInitBearerResModRequest": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-detach": {
    "detachRequest": {
      "": {
        "Value": 3
      },
      "IMSIPLMN:20893": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "detachRequestMmeInit": {
      "": {
        "Value": 0
      }
    },
    "detachSuccess": {
      "": {
        "Value": 0
      }
    },
    "detachSuccessMmeInit": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-interface": {
    "eNbConfigurationUpdateRequest": {
      "": {
        "Value": 0
      }
    },
    "eNbConfigurationUpdateSuccess": {
      "": {
        "Value": 0
      }
    },
    "eNodeBInitS1ResetRequest": {
      "": {
        "Value": 0
      }
    },
    "eNodeBInitS1ResetSuccess": {
      "": {
        "Value": 0
      }
    },
    "eRabModificationConfirm": {
      "": {
        "Value": 0
      }
    },
    "eRabModificationIndication": {
      "": {
        "Value": 0
      }
    },
    "eRabModifyRequest": {
      "": {
        "Value": 0
      }
    },
    "eRabModifyResponse": {
      "": {
        "Value": 0
      }
    },
    "eRabReleaseCommand": {
      "": {
        "Value": 0
      }
    },
    "eRabReleaseResponse": {
      "": {
        "Value": 0
      }
    },
    "eRabSetupRequest": {
      "": {
        "Value": 0
      }
    },
    "initialContextSetupFailure": {
      "": {
        "Value": 0
      }
    },
    "initialContextSetupRequest": {
      "": {
        "Value": 0
      }
    },
    "initialContextSetupResponse": {
      "": {
        "Value": 0
      }
    },
    "mmeConfigurationUpdateRequest": {
      "": {
        "Value": 0
      }
    },
    "mmeConfigurationUpdateSuccess": {
      "": {
        "Value": 0
      }
    },
    "s1SetupRequest": {
      "": {
        "Value": 4
      },
      "TAI:001010001": {
        "Value": 1
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "s1SetupSuccess": {
      "": {
        "Value": 4
      },
      "TAI:001010001": {
        "Value": 1
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "ueContextModificationFailure": {
      "": {
        "Value": 0
      }
    },
    "ueContextModificationRequest": {
      "": {
        "Value": 0
      }
    },
    "ueContextModificationResponse": {
      "": {
        "Value": 0
      }
    },
    "ueContextReleaseCommand": {
      "": {
        "Value": 8
      },
      "TAI:208930001": {
        "Value": 8
      }
    },
    "ueContextReleaseCommand1_20": {
      "": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "ueContextReleaseCommand3_0": {
      "": {
        "Value": 2
      },
      "TAI:208930001": {
        "Value": 2
      }
    },
    "ueContextReleaseCommand3_2": {
      "": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "ueContextReleaseComplete": {
      "": {
        "Value": 8
      },
      "TAI:208930001": {
        "Value": 8
      }
    },
    "ueContextReleaseRequest": {
      "": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "ueContextReleaseRequest1_20": {
      "": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    }
  },
  "S11-interface": {
    "bearerResourceCommand": 0,
    "bearerResourceFailureIndication": 0,
    "createBearerRequest": 0,
    "createBearerResponse": 0,
    "createIndirectDataForwardingTunnelRequest": 0,
    "createIndirectDataForwardingTunnelResponse": 0,
    "createSessionRequest": 4,
    "createSessionResponse": 4,
    "deleteBearerCommand": 0,
    "deleteBearerFailureIndication": 0,
    "deleteBearerRequest": 0,
    "deleteBearerResponse": 0,
    "deleteIndirectDataForwardingTunnelRequest": 0,
    "deleteIndirectDataForwardingTunnelResponse": 0,
    "deleteSessionRequest": 5,
    "deleteSessionResponse": 5,
    "downlinkDataNotification": 0,
    "downlinkDataNotificationAck": 0,
    "downlinkDataNotificationFailureInd": 0,
    "modifyBearerCommand": 0,
    "modifyBearerFailureIndication": 0,
    "modifyBearerRequest": 0,
    "modifyBearerResponse": 0,
    "releaseAccessBearersRequest": 3,
    "releaseAccessBearersResponse": 3,
    "suspendAcknowledge": 0,
    "suspendNotification": 0,
    "updateBearerRequest": 0,
    "updateBearerResponse": 0
  },
  "S6a-interface": {
    "authenticationInformationAnswer": 4,
    "authenticationInformationAnswer2001": 4,
    "authenticationInformationAnswer3004": 0,
    "authenticationInformationAnswer5001": 0,
    "authenticationInformationAnswer5420": 0,
    "authenticationInformationRequest": 4,
    "cancelLocationAnswer": 0,
    "cancelLocationAnswer2001": 0,
    "cancelLocationRequest": 0,
    "deleteSubscriberDataAnswer": 0,
    "deleteSubscriberDataRequest": 0,
    "insertSubscriberDataAnswer": 0,
    "insertSubscriberDataRequest": 0,
    "notifyAnswer": 4,
    "notifyAnswer2001": 4,
    "notifyRequest": 4,
    "purgeUeAnswer": 0,
    "purgeUeAnswer2001": 0,
    "purgeUeRequest": 0,
    "resetAnswer": 0,
    "resetRequest": 0,
    "updateLocationAnswer": 2,
    "updateLocationAnswer2001": 2,
    "updateLocationRequest": 2
  },
  "SGs-interface": {
    "sGsApLocationUpdateAccept": {
      "": {
        "Value": 0
      }
    },
    "sGsApLocationUpdateRequest": {
      "": {
        "Value": 0
      }
    }
  },
  "Sv-interface": {
    "srvccPsToCsCompleteAcknowledge": 0,
    "srvccPsToCsCompleteNotification": 0,
    "srvccPsToCsRequest": 0,
    "srvccPsToCsResponse": 0
  },
  "TAU": {
    "interCombinedTauRequest": {
      "": {
        "Value": 0
      }
    },
    "interCombinedTauSuccess": {
      "": {
        "Value": 0
      }
    },
    "interTauRequest": {
      "": {
        "Value": 0
      }
    },
    "interTauSuccess": {
      "": {
        "Value": 0
      }
    },
    "intraCombinedTauRequest": {
      "": {
        "Value": 0
      }
    },
    "intraCombinedTauSuccess": {
      "": {
        "Value": 0
      }
    },
    "intraTauRequest": {
      "": {
        "Value": 0
      }
    },
    "intraTauSuccess": {
      "": {
        "Value": 0
      }
    },
    "periodTauRequest": {
      "": {
        "Value": 2
      },
      "TAI:208930001": {
        "Value": 2
      }
    },
    "periodTauSuccess": {
      "": {
        "Value": 2
      },
      "TAI:208930001": {
        "Value": 2
      }
    }
  },
  "Users": {
    "realTimeAttachedUsersAtEcmConnectedStatus": {
      "": {
        "Value": 1
      },
      "IMSIPLMN:20893": {
        "Value": 1
      },
      "TAI:208930001": {
        "Value": 1
      }
    },
    "realTimeAttachedUsersAtEcmIdleStatus": {
      "": {
        "Value": 4
      },
      "IMSIPLMN:20893": {
        "Value": 4
      },
      "TAI:208930001": {
        "Value": 4
      }
    },
    "realTimeDedicatedBearerNumber": {
      "": {
        "Value": 0
      }
    },
    "realTimePdnConnectionNumber": {
      "": {
        "Value": 0
      }
    },
    "realTimeUsersAtEmmDeregisteredStatus": {
      "": {
        "Value": 5
      },
      "IMSIPLMN:20893": {
        "Value": 5
      },
      "TAI:208930001": {
        "Value": 5
      }
    }
  }
}
```

### Получить информацию об абоненте, get_profile {#get_profile}

#### Запрос

```bash
$ ./get_profile {imsi}
```

#### Поля запроса

| Поле | Описание             | Тип    | O/M |
|------|----------------------|--------|-----|
| imsi | Номер IMSI абонента. | string | M   |

#### Пример запроса

```bash
$ ./get_profile 001010000000398
```

#### Пример ответа

```json5
{
  "5G_EA": null,
  "5G_IA": null,
  "A-MSISDN": null,
  "ARD": 0,
  "AWoPDN": false,
  "ActivityDetected": null,
  "CP_CIoT_opt": true,
  "CellID": 917760,
  "DRX_param": null,
  "EEA": 240,
  "EIA": 112,
  "EMM_State": "DEREGISTERED",
  "ENB_IP": "172.30.37.58",
  "ENB_UE_ID": null,
  "EUTRAN_Trace_ID": null,
  "E_DRX_param": "eDRX value 2, Paging Time Window 0",
  "EmergencyAttach": false,
  "GEA": null,
  "GTP_C_TEID_SELF": null,
  "GUTI": "99999;1;1;339",
  "GUTI_LastReallocTimestamp": "2023-09-16_15:07:22",
  "GUTI_ReallocTrigger_count": 1,
  "HSS_Host": "hss.epc.mnc048.mcc250.3gppnetwork.org",
  "HSS_reset": null,
  "IMEISV": null,
  "IMSI": "001010000000398",
  "MME_UE_ID": null,
  "MSISDN": "76000000398",
  "MS_Classmark2": null,
  "MS_Classmark3": null,
  "NCC": null,
  "NEAF": null,
  "NH": null,
  "ODB": null,
  "PDN_Conn_restricted": false,
  "PDN_Manager": {
    "Barebone #1": {
      "Bearer": {
        "ARP": "A&R priority level 10, may trigger preemption, not preemptable",
        "QCI": 6,
        "id": "0",
        "type": "default"
      },
      "Connectivity": {
        "APN": "ims.mnc000.mcc101.gprs",
        "APN AMBR": {
          "DL": "100000000 b/sec",
          "UL": "100000000 b/sec"
        },
        "HSS Conconsole": "1",
        "PAA": "no IPv4, no IPv6",
        "PDN Type": "ipv4",
        "S11 UL F-TEID": "0:0.0.0.0-::"
      }
    }
  },
  "PLMN": "00101",
  "RFSP_id": null,
  "TAC": 1,
  "TCE_IP": null,
  "UEA": 0,
  "UE_radio_cap": null,
  "UE_radio_cap_for_paging": null,
  "UIA": 0,
  "dl_NAS_count": 3,
  "dl_UE_AMBR": 1024000000,
  "knasenc_type": 0,
  "knasint_type": 2,
  "ul_NAS_count": 0,
  "ul_UE_AMBR": 1024000000
}
```

### Получить информацию о состоянии GTP-соединений на MME, gtp_peers {#gtp_peers}

#### Запрос

```bash
$ ./gtp_peers
```

#### Ответ

```text
{
  "gtpPeers": {
    "<iface_name>": [
      {
        "ip": <ip>,
        "restartCounter": <restart_counter>,
        "state": <connection_state>,
        "version": <gtp_version>
      }
    ]
  },
  "selfRestartCounter": <node_restart_counter>
}
```

#### Поля ответа

| Поле               | Описание                                                                                                                                                | Тип            | O/M |
|--------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|-----|
| gtpPeers           | Перечень соединений.                                                                                                                                    | <object\>      | M   |
| selfRestartCounter | Значение счетчика перезапусков узла. См. [3GPP TS 23.007](https://www.etsi.org/deliver/etsi_ts/123000_123099/123007/17.05.01_60/ts_123007v170501p.pdf). | int            | M   |
| <iface_name>       | Перечень параметров соединений по интерфейсу.                                                                                                           | list\<object\> | M   |
| ip                 | IP-адрес соединения.                                                                                                                                    | ip             | M   |
| restartCounter     | Значение счетчика перезапусков. См. [3GPP TS 23.007](https://www.etsi.org/deliver/etsi_ts/123000_123099/123007/17.05.01_60/ts_123007v170501p.pdf).      | int            | M   |
| state              | Флаг активности соединения.                                                                                                                             | bool           | M   |
| version            | Версия протокола GTP.                                                                                                                                   | string         | M   |

#### Пример ответа

```json5
{
  "gtpPeers": {
    "S10": [
      {
        "ip": "192.168.125.182",
        "restartCounter": 5,
        "state": false,
        "version": 2
      }
    ],
    "S11": [
      {
        "ip": "192.168.126.11",
        "restartCounter": null,
        "state": true,
        "version": 2
      },
      {
        "ip": "192.168.125.95",
        "restartCounter": null,
        "state": true,
        "version": 2
      }
    ]
  },
  "selfRestartCounter": 36
}
```

### Получить информацию о состоянии базовых станций LTE на MME, s1_peers {#s1_peers}

#### Запрос

```bash
$ ./s1_peers [ip={ip}] [id={id}] [plmn={plmn}] [name={name}]
```

#### Поля запроса

| Поле | Описание                                       | Тип    | O/M |
|------|------------------------------------------------|--------|-----|
| ip   | IP-адрес базовой станции.                      | ip     | C   |
| id   | Идентификатор базовой станции.                 | string | C   |
| plmn | Код PLMN, которой принадлежит базовая станция. | string | C   |
| name | Имя базовой станции.                           | string | C   |

#### Пример запроса

```bash
$ ./s1_peers plmn 99999
```

#### Ответ

```text
[
  {
    "PLMN": <plmn_id>,
    "TAC": <tac>,
    "id": <enodeb_id>,
    "ip": <enodeb_ip>,
    "name": <enodeb_name>,
    "state": <enodeb_state>
  }
]
```

#### Поля ответа

| Поле  | Описание                  | Тип    | O/M |
|-------|---------------------------|--------|-----|
| PLMN  | Идентификатор сети PLMN.  | string | M   |
| TAC   | Код области отслеживания. | int    | M   |
| id    | Идентификатор eNodeB.     | string | M   |
| ip    | IP-адрес eNodeB.          | string | M   |
| name  | Имя eNodeB.               | string | M   |
| state | Флаг активности eNodeB.   | bool   | M   |

#### Пример ответа

```json5
{
  "PLMN": "99999",
  "TAC": 1,
  "id": "00101-176849",
  "ip": "172.30.153.1",
  "name": "enb2b2d1",
  "state": true
}
```

### Получить информацию о состоянии базовых станций LTE на MME в табличном виде, s1_table {#s1_table}

#### Запрос

```bash
$ ./s1_table
```

#### Ответ

```console
PLMN    TAC  eNB-ID    IP        Name        State
——————— ———— ————————— ————————— ——————————— ————————————
plmn_id tac  enodeb_id enodeb_ip enodeb_name enodeb_state
```

#### Поля ответа

| Поле   | Описание                  | Тип    | O/M |
|--------|---------------------------|--------|-----|
| PLMN   | Идентификатор сети PLMN.  | string | M   |
| TAC    | Код области отслеживания. | int    | M   |
| ENB-ID | Идентификатор eNodeB.     | string | M   |
| IP     | IP-адрес eNodeB.          | ip     | M   |
| Name   | Имя eNodeB.               | string | M   |
| State  | Флаг активности eNodeB.   | bool   | M   |

#### Пример ответа

```console
PLMN  TAC  eNB-ID       IP           Name     State
————— ———— ———————————— ———————————— ———————— —————
00101 1    00101-176849 172.30.153.1 enb2b2d1 false
99912 1    00101-176849 172.30.153.1 enb2b2d1 false
99999 1    00101-176849 172.30.153.1 enb2b2d1 false
```

### Получить информацию о состоянии SGs-соединений, sgs_peers {#sgs_peers}

#### Запрос

```bash
$ ./sgs_peers
```

#### Ответ

```console
% Total  % Received  % Xferd  Average   Speed    Time    Time     Time     Current
                              Dload    Upload    Total   Spent    Left     Speed
% total  %     rcvd  % dlvrd   avg_dl rate_ul  hh:mm:ss hh:mm:ss hh:mm:ss rate_now
{
  "sgsPeers": [
    {
      "blocked": <connection_state>,
      "gt": <global_title>,
      "ip": <ip>,
      "state": <activity_state>
    }
  ]
}
```

#### Поля ответа

| Поле     | Описание                    | Тип            | O/M |
|----------|-----------------------------|----------------|-----|
| sgsPeers | Перечень соединений.        | list\<object\> | M   |
| blocked  | Флаг блокировки соединения. | bool           | M   |
| gt       | Глобальный заголовок узла.  | string         | M   |
| ip       | IP-адрес соединения.        | ip             | M   |
| state    | Флаг активности соединения. | string         | M   |

#### Пример ответа

```console
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   127  100   127    0     0   3968      0 --:--:-- --:--:-- --:--:--  3968
{
  "sgsPeers": [
    {
      "blocked": false,
      "gt": "79216567568",
      "ip": "192.168.125.154",
      "state": true
    }
  ]
}
```

### Управление отслеживанием абонента, trace_UE {#trace_UE}

Файлы с данными отслеживания находятся в директории
**/usr/protei/Protei_MME/logs/pcap_trace**.

Информация для указанного абонента хранится в файле
**/usr/protei/log/Protei_MME/pcap_trace/{IMSI}_%Y-%m-%d_%H:%M:%s.pcap**.

#### Запрос на добавление отслеживания абонента

```bash
$ ./trace_UE {IMSI} add
$ ./trace_UE {MSISDN} add
```

#### Запрос на прекращение отслеживания абонента

```bash
$ ./trace_UE {IMSI} delete
$ ./trace_UE {MSISDN} delete
```

#### Поля ответа

| Поле   | Описание               | Тип    | O/M |
|--------|------------------------|--------|-----|
| IMSI   | Номер IMSI абонента.   | string | C   |
| MSISDN | Номер MSISDN абонента. | string | C   |

#### Ответ

```console
<status>
```

#### Пример запроса на добавление отслеживания абонента

```bash
$ ./trace_UE 001010000000311 add
```

#### Пример запроса на прекращение отслеживания абонента

```bash
$ ./trace_UE 001010000000311 delete
```
#### Пример ответа

```console
Done
```