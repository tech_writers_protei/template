---
title: "Скрипты командной строки"
description: "Скрипты bash для методов HTTP API"
weight: 35
type: docs
---

В этом разделе в качестве примеров обращения к API используются bash-скрипты, которые находятся в директории
`/usr/protei/Protei_MME/scripts/` на серверах MME.

Описание скриптов и примеры применения можно получить в стандартном
выводе при запуске скрипта без указания входных параметров.

* [clear_dns_cache](#clear_dns_cache) — очистить cache адресов DNS;
* [deact_bearer](#deact_bearer) — деактивировать bearer-службу;
* [detach](#detach) — удалить регистрацию абонента из сети;
* [detach_by_vlr](#detach_by_vlr) — удалить регистрации абонентов на определенном VLR и запретить новые;
* [diam_state](#diam_state) — получить информацию о состоянии PCSM-узлов; 
* [enable_vlr](#enable_vlr) — разрешить регистрации на определенном VLR;
* [enb_disconnect](#enb_disconnect) — разорвать соединение с eNodeB;
* [get_bearers](#get_bearers) — получить информацию о bearer-службах;
* [get_location](#get_location) — получить местоположение абонента;
* [get_metrics](#get_metrics) — получить текущие значения метрик;
* [get_profile](#get_profile) — получить информацию об абоненте;
* [gtp_peers](#gtp_peers) — получить информацию о состоянию GTP-соединений;
* [s1_peers](#s1_peers) — получить информацию о состоянии базовых станций LTE из хранилища MME;
* [s1_table](#s1_table) — получить информацию о состоянии базовых станций LTE из хранилища MME в табличном виде;
* [sgs_peers](#sgs_peers) — получить информацию о состоянии SGs-соединений;
* [trace_UE](#trace_ue) — добавить/удалить трассировку на eNodeB для абонента.
