---
title: s1_peers
description: Получить информацию о состоянии базовых станций LTE на MME
weight: 30
type: docs
---

### Запрос

```bash
$ ./s1_peers [ip={ip}] [id={id}] [plmn={plmn}] [name={name}]
```

### Поля запроса

| Поле | Описание                                       | Тип    | O/M |
|------|------------------------------------------------|--------|-----|
| ip   | IP-адрес базовой станции.                      | ip     | C   |
| id   | Идентификатор базовой станции.                 | string | C   |
| plmn | Код PLMN, которой принадлежит базовая станция. | string | C   |
| name | Имя базовой станции.                           | string | C   |

### Пример запроса

```bash
$ ./s1_peers plmn 99999
```

### Ответ

```text
[
  {
    "PLMN": <plmn_id>,
    "TAC": <tac>,
    "id": <enodeb_id>,
    "ip": <enodeb_ip>,
    "name": <enodeb_name>,
    "state": <enodeb_state>
  }
]
```

#### Поля ответа

| Поле  | Описание                  | Тип    | O/M |
|-------|---------------------------|--------|-----|
| PLMN  | Идентификатор сети PLMN.  | string | M   |
| TAC   | Код области отслеживания. | int    | M   |
| id    | Идентификатор eNodeB.     | string | M   |
| ip    | IP-адрес eNodeB.          | string | M   |
| name  | Имя eNodeB.               | string | M   |
| state | Флаг активности eNodeB.   | bool   | M   |

#### Пример ответа

```json5
{
  "PLMN": "99999",
  "TAC": 1,
  "id": "00101-176849",
  "ip": "172.30.153.1",
  "name": "enb2b2d1",
  "state": true
}
```