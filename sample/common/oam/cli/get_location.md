---
title: get_location
description: Получить местоположение абонента
weight: 30
type: docs
---

### Запрос

```bash
$ ./get_location {imsi}
```

### Поля запроса

| Поле | Описание             | Тип    | O/M |
|------|----------------------|--------|-----|
| imsi | Номер IMSI абонента. | string | M   |

### Пример запроса

```bash
$ ./get_location 001010000000390
```

### Ответ

```text
{
  "CellID": <cell_id>,
  "ENB-ID": <enodeb_id>,
  "EPS_State": <eps_state>,
  "PLMN": <plmn_id>,
  "TAC": <tac>
}
```

### Поля ответа ###

| Поле                    | Описание                    | Тип    | O/M |
|-------------------------|-----------------------------|--------|-----|
| CellID                  | Идентификатор соты.         | hex    | M   |
| ENB-ID                  | Идентификатор eNodeB.       | string | M   |
| [EPS_State](#eps_state) | Код состояния абонента EPS. | int    | M   |
| PLMN                    | Идентификатор сети PLMN.    | string | M   |
| TAC                     | Код области отслеживания.   | int    | M   |

### Значения EPS State {#eps_state}

Полное описание см. [3GPP TS 23.078](https://www.etsi.org/deliver/etsi_ts/123000_123099/123078/17.00.00_60/ts_123078v170000p.pdf).

* `0` - Detached;
* `1` - AttachedNotReachableForPaging;
* `2` - AttachedReachableForPaging;
* `3` - ConnectedNotReachableForPaging;
* `4` - ConnectedReachableForPaging;
* `5` - NotProvidedFromSGSN;

### Пример ответа

```json5
{
  "CellID": 45273345,
  "ENB-ID": "00101-176849",
  "EPS_State": 4,
  "PLMN": "00101",
  "TAC": 1
}
```