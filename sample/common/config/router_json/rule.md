---
title: "Rule"
description: "Параметры правила Rule"
weight: 30
type: docs
---

**Rule** задает правила маршрутизации сообщений.

### Описание параметров ###

| Параметр                                    | Описание                                                                                                                                                                                                                                                                                          | Тип          | O/M | P/R | Версия |
|---------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|-----|-----|--------|
| ID                                          | Уникальный идентификатор правила маршрутизации.                                                                                                                                                                                                                                                   | int          | O   | R   |        |
| Name                                        | Уникальное имя правила маршрутизации.                                                                                                                                                                                                                                                             | string       | M   | R   |        |
| Type                                        | Тип узла маршрутизации. Должно быть `Rule`.                                                                                                                                                                                                                                                       | string       | M   | R   |        |
| Category                                    | Категория правила. См. [Классификация сообщений](../../../basics/functionalities/#msg_category/).<br>Диапазон: 1-3.                                                                                                                                                                               | int          | O   | R   |        |
| AttackType                                  | Вид атаки.                                                                                                                                                                                                                                                                                        | string       | O   | R   |        |
| Key                                         | Перечень ключей для задания адресации.                                                                                                                                                                                                                                                            | \[object\]   | O   | R   |        |
| **{**                                       |                                                                                                                                                                                                                                                                                                   |              |     |     |        |
| &nbsp;&nbsp;ID                              | Уникальный идентификатор условия.                                                                                                                                                                                                                                                                 | int          | O   | R   |        |
| &nbsp;&nbsp;<a name="field">Field</a>       | Анализируемый параметр.                                                                                                                                                                                                                                                                           | string       | O   | R   |        |
| &nbsp;&nbsp;<a name="operator">Operator</a> | Оператор, задающий отношение для параметра [Field](#field).                                                                                                                                                                                                                                       | string       | O   | R   |        |
| &nbsp;&nbsp;<a name="value">Value</a>       | Значение, используемое оператором [Operator](#operator) для сравнения или проверки параметра [Field](#field).                                                                                                                                                                                     | string/regex | O   | R   |        |
| &nbsp;&nbsp;Type                            | Тип значения [Value](#value).<br>`Int` - числовой тип;<br>`String` - строковый тип;<br>`Field` - название параметра;<br>`List` - имя файла с префиксами;<br>`Vocabulary` - имя файла с перечнем стоп-слов;<br>`Tag` - название тега.                                                              | string       | O   | R   |        |
| &nbsp;&nbsp;Condition                       | Условие связи с предыдущим блоком.<br>`AND` - логическое И;<br>`OR` - логическое ИЛИ;<br>`NOT` - логическое НЕ. `<Condition1> NOT <Condition2> = <Condition1> AND (NOT <Condition2>)`<br>По умолчанию: AND.<br>**Примечание.** При задании первого блока считается, что нулевой блок всегда True. | string       | O   | R   |        |
| &nbsp;&nbsp;Tag                             | Теги сообщения.                                                                                                                                                                                                                                                                                   | \[string\]   | O   | R   |        |
| &nbsp;&nbsp;Option                          | Дополнительный атрибут параметра [Field](#field).<br>`Length` - количество символов;<br>`Prefix` - префикс;<br>`Postfix` - постфикс;<br>`Network` - сетевая зона.                                                                                                                                 | string       | O   | R   |        |
| &nbsp;&nbsp;SizeOfPrefix                    | Длина префикса.<br>**Примечание.** Используется только при `Option = Prefix`.                                                                                                                                                                                                                     | int          | O   | R   |        |
| &nbsp;&nbsp;Start                           | Порядковый номер символа, с которого начинается постфикс.<br>**Примечание.** Используется только при `Option = Postfix`.                                                                                                                                                                          | int          | O   | R   |        |
| **}**                                       |                                                                                                                                                                                                                                                                                                   |              |     |     |        |
| Threshold                                   | Параметры порога.                                                                                                                                                                                                                                                                                 | object       |     |     |        |
| **{**                                       |                                                                                                                                                                                                                                                                                                   |              |     |     |        |
| &nbsp;&nbsp;Param                           | Параметр, для которого устанавливается пороговое значение.                                                                                                                                                                                                                                        | string       | O   | R   |        |
| &nbsp;&nbsp;Limit                           | Пороговое значение.                                                                                                                                                                                                                                                                               | int          | O   | R   |        |
| &nbsp;&nbsp;Interval                        | Интервал проверки достижения порогового значения, в секундах.                                                                                                                                                                                                                                     | int          | O   | R   |        |
| *}*                                         |                                                                                                                                                                                                                                                                                                   |              |     |     |        |
| [Actions](#actions)                         | Дальнейшие осуществляемые действия.                                                                                                                                                                                                                                                               | \[object\]   | M   | R   |        |
| **{**                                       |                                                                                                                                                                                                                                                                                                   |              |     |     |        |
| &nbsp;&nbsp;Name                            | Название действия.                                                                                                                                                                                                                                                                                | string       | O   | R   |        |
| &nbsp;&nbsp;Data                            | Дополнительная информация.                                                                                                                                                                                                                                                                        | object       | O   | R   |        |
| *}*                                         |                                                                                                                                                                                                                                                                                                   |              |     |     |        |
| &nbsp;&nbsp;Next                            | Название узла, куда перенаправляется запрос.<br>**Примечание.** Должно совпадать с именем существующего правила / маршрутизатора / "OUT".                                                                                                                                                         | string       | M   | R   |        |
| *}*                                         |                                                                                                                                                                                                                                                                                                   |              |     |     |        |

### Операторы для анализа параметров {#operators}

| Оператор                | Описание                                                                                                                   |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `==` / `!=`             | Оператор "равно" используется для сравнения значения параметра с точным значением или значением другого параметра.         | 
| `~` / `!~`              | Оператор "эквивалентно" используется для проверки попадания под условия маски.                                             |
| `in`                    | Оператор "включено" используется для проверки наличия значения в списке или словаре.                                       |
| `>` / `<` / `>=` / `<=` | Оператор "меньше/больше" используется для сравнения значения параметра с точным значением или значением другого параметра. |

### Доступные действия {#actions}

#### Передать сообщение в сеть без обработки, Transmit

```json
{ "Name": "Transmit" }
```

#### Остановить обработку сообщения, сделать запрос к базе данных для получения текущей сетевой зоны абонента и продолжить обработку, Get current network area

```json
{ "Name": "get_current_network_area" }
```

#### Прекратить обработку сообщения и ответить отбоем, Reject

```json
{ "Name": "Reject" }
```

#### Прекратить обработку сообщения и ответить сообщением TCAP_RETURN_ERROR с кодом ошибки ErrorCode, ReturnError

```json
{
  "Name": "ReturnError",
  "Data": {
    "ErrorCode": 0
  }
}
```

| Параметр                 | Описание                 | Тип |
|--------------------------|--------------------------|-----|
| [ErrorCode](#error_code) | Код возвращаемой ошибки. | int |

<a name="error_code">Возможные коды ошибки</a>

| Код | Ошибка                      | Описание                                        |
|-----|-----------------------------|-------------------------------------------------|
| 0   | ORIGINAL_ERROR              | Успех                                           |
| 1   | UNKNOWN_SUBSCRIBER          | Абонент неизвестен                              |
| 5   | UNIDENTIFIED_SUBSCRIBER     | Абонент не опознан                              |
| 6   | ABSENT_SUBSCRIBER_SM        | Сообщение абонента отсутствует                  |
| 8   | ROAMING_NOT_ALLOWED         | Роуминг запрещен                                |
| 9   | ILLEGAL_SUBSCRIBER          | Абонент не зарегистрирован                      |
| 11  | TELESERVICE_NOT_PROVISIONED | Телеуслуга не может быть предоставлена          |
| 12  | ILLEGAL_EQUIPMENT           | Устройство не зарегистрировано                  |
| 13  | CALL_BARRED                 | Вызов прерван ввиду ограничений                 |
| 21  | FACILITY_NOT_SUPPORTED      | Услуга не поддерживается                        |
| 31  | SUBSCRIBER_BUSY_FOR_MT_SMS  | Сообщение не получено абонентом ввиду занятости |
| 32  | SM_DELIVERY_FAILURE         | Сообщение не доставлено                         |
| 34  | SYSTEM_ERROR                | Внутренняя ошибка                               |
| 35  | DATA_MISSING                | Недостаточно информации                         |
| 36  | UNEXPECTED_DATA_VALUE       | Получено не ожидаемое значение                  |

#### Прекратить обработку сообщения и ответить сообщением TCAP_RETURN_RESULT, ReturnResult

```json
{ "Name": "ReturnResult" }
```

#### Отправить запрос MAP-Any-Time-Interrogation, SendATI

```json
{
  "Name": "SendATI",
  "Data": {
    "EnablePaging": 1,
    "ValidityPeriod": 10000
  }
}
```

| Параметр       | Описание                               | Тип  |
|----------------|----------------------------------------|------|
| EnablePaging   | Флаг включения пейджинга.              | bool |
| ValidityPeriod | Период годности сообщения, в секундах. | int  |

#### Отправить оповещение с аварией и продолжить обработку сообщения, SendAlarm

```json
{ "Name": "SendAlarm" }
```

#### Задать сообщению тег, SetTag

```json
{
  "Name": "SetTag",
  "Data": {
    "TagName1": "tag_1",
    "TagNameN": "tag_N"
  }
}
```

| Параметр | Описание           | Тип    |
|----------|--------------------|--------|
| TagName  | Наименование тега. | string |

#### Прекратить обработку без отправки ответа, SilentDrop

```json
{ "Name": "SilentDrop" }
```

#### Перейти к определенному правилу, GOTO

```json
{
  "Name": "GOTO",
  "Data": "<rule_name>"
}
```

#### Задать параметру значение, SetParam

```json
{
  "Name": "SetParam",
  "Data": {
    "paramName": {
      "variable": "GT_B_Address"
    },
    "valueType": "Field",
    "paramValueField": {
      "variable": "GT_A_Address"
    },
    "operator": "DelPrefix",
    "operatorValueString": {
      "VAL": "5"
    }
  }
}
```

| Параметр                            | Описание                                                                                                        | Тип    |
|-------------------------------------|-----------------------------------------------------------------------------------------------------------------|--------|
| paramName                           | Имя параметра. Формат: `"paramName": { "variable": "<variable>" }`.                                             | object |
| <a name="value">paramValueField</a> | Имя параметра, значение которого используется. Формат: `"paramValueField": { "variable": "<variable>" }`.       | object |
| variable                            | Имя переменной.                                                                                                 | string |
| valueType                           | Тип значения. `String` / `Int` / `Field`.                                                                       | string |
| paramValueString                    | Задаваемое значение параметра.                                                                                  | string |
| <a name="operator">operator</a>     | Действие со значением [paramValueField](#value). `DelPrefix` / `AddPrefix`.                                     | string |
| operatorValueString                 | Дополнительные параметры действия [operator](#operator). Формат: `"operatorValueString": { "VAL": "<value>" }`. | object |
| VAL                                 | Значение параметра действия [operator](#operator).                                                              | string |

#### Проверить скорость перемещения, VelocityCheck

```json
{
  "Name": "VelocityCheck",
  "Data": {
    "AtiAllowed": 1,
    "EnablePaging": 1,
    "ValidityPeriod": 10000
  }
}
```

| Параметр       | Описание                                            | Тип  |
|----------------|-----------------------------------------------------|------|
| AtiAllowed     | Флаг разрешения запроса MAP-Any-Time-Interrogation. | bool |
| EnablePaging   | Флаг включения пейджинга.                           | bool |
| ValidityPeriod | Время жизни сообщения, в секундах.                  | int  |

#### Преобразовать данные, ConvertTo

```json
{
  "Name": "ConvertTo",
  "Data": {
    "Type": 1,
    "SMPP_Direction": 0
  }
}
```

| Параметр       | Описание                                                      | Тип |
|----------------|---------------------------------------------------------------|-----|
| Type           | Код типа данных, к которому необходимо конвертировать данные. | int |
| SMPP_Direction | Идентификатор направления SMPP.                               | int |

### Параметры, доступные для анализа

| Name                                  | Description                                                                  | Тип             | Версия     |
|---------------------------------------|------------------------------------------------------------------------------|-----------------|------------|
| GT_A_Address                          | Адрес SCCP <abbr title="Global Title">GT</abbr> отправителя.                 | regex           |            |
| GT_A_NP                               | Номер <abbr title="Numbering Plan">NP</abbr> отправителя.                    | int             |            |
| GT_A_SSN                              | Номер <abbr title="Subsystem Number">SSN</abbr> отправителя.                 | int             |            |
| GT_A_TT                               | Номер <abbr title="Translation Type">TT</abbr> отправителя.                  | int             |            |
| GT_A_NAI                              | Номер <abbr title="Nature of Address Indicator">NAI</abbr> отправителя.      | int             |            |
| GT_A_ES                               | Номер <abbr title="Encoding Scheme">ES</abbr> отправителя.                   | int             |            |
| GT_B_Address                          | Адрес SCCP <abbr title="Global Title">GT</abbr> получателя.                  | regex           |            |
| GT_B_NP                               | Номер <abbr title="Numbering Plan">NP</abbr> получателя.                     | int             |            |
| GT_B_SSN                              | Номер <abbr title="Subsystem Number">SSN</abbr> получателя.                  | int             |            |
| GT_B_TT                               | Номер <abbr title="Translation Type">TT</abbr> получателя.                   | int             |            |
| GT_B_NAI                              | Номер <abbr title="Nature of Address Indicator">NAI</abbr> получателя.       | int             |            |
| GT_B_ES                               | Номер <abbr title="Encoding Scheme">ES</abbr> получателя.                    | int             |            |
| MAP_IMSI                              | Номер IMSI в MAP-запросах.                                                   | regex           |            |
| MAP_MSISDN                            | Номер MSISDN в MAP-запросах.                                                 | regex           |            |
| MAP_SMSC                              | Адрес узла SMSC в MAP-запросах.                                              | regex           |            |
| MAP_MSC                               | Адрес узла MSC в MAP-запросах.                                               | regex           |            |
| MAP_VLR                               | Адрес узла VLR в MAP-запросах.                                               | regex           |            |
| MAP_SGSN                              | Адрес узла SGSN в MAP-запросах.                                              | regex           |            |
| MAP_AC                                | Имя контекста приложения, <abbr title="Application Context Name">ACN</abbr>. | string          |            |
| MAP_REG_PARAM                         | Параметры Request Parameters.                                                | string          |            |
| MAP_OCSI_SCF                          | Адрес gsmSCF с профилем O-CSI.                                               | regex           |            |
| MAP_HLR                               | Адрес узла HLR.                                                              | regex           |            |
| CAP_IMSI                              | Номер IMSI в СAP-запросах.                                                   | regex           |            |
| CAP_VLR                               | Адрес узла VLR в СAP-запросах.                                               | regex           |            |
| CAP_MSC                               | Адрес узла MSC в СAP-запросах.                                               | regex           |            |
| CAP_MSISDN                            | Номер MSISDN в СAP-запросах.                                                 | regex           |            |
| CAP_GSMSCF                            | Адрес gsmSCF.                                                                | regex           |            |
| CAP_SERVICEKEY                        | Код ServiceKey.                                                              | int             | 1.0.9.2    |
| CAP_EVENTTYPEBCSM                     | Код EventTypeBCSM.                                                           | int             | 1.0.9.2    |
| MTP3_OPC                              | Код Origination Point Code.                                                  | regex           |            |
| MTP3_DPC                              | Код Destination Point Code.                                                  | regex           |            |
| IP_Src                                | IP-адрес источника в формате CIDR.                                           | ip/\[ip\]       | 1.0.12.0   |
| IP_Dst                                | IP-адрес назначения в формате CIDR.                                          | ip/\[ip\]       | 1.0.12.0   |
| Src_Port                              | Порт источника.                                                              | int             | 1.0.12.0   |
| Dst_Port                              | Порт назначения.                                                             | int             | 1.0.12.0   |
| DIAM_UserName                         | Значение Diameter AVP: `User-Name`.                                          | string          | 1.0.4.0    |
| DIAM_OH                               | Значение Diameter AVP: `Origin-Host`.                                        | regex           | 1.0.4.0    |
| DIAM_OR                               | Значение Diameter AVP: `Origin-Realm`.                                       | regex           | 1.0.4.0    |
| DIAM_DH                               | Значение Diameter AVP: `Destination-Host`.                                   | regex           | 1.0.4.0    |
| DIAM_DR                               | Значение Diameter AVP: `Destination-Realm`.                                  | regex           | 1.0.4.0    |
| DIAM_OH_MCCMNC                        | Значение MCC + MNC для `Origin-Host`.                                        | string          | 1.0.4.0    |
| DIAM_OR_MCCMNC                        | Значение MCC + MNC для `Origin-Realm`.                                       | string          | 1.0.4.0    |
| DIAM_DH_MCCMNC                        | Значение MCC + MNC для `Destination-Host`.                                   | string          | 1.0.4.0    |
| DIAM_DR_MCCMNC                        | Значение MCC + MNC для `Destination-Realm`.                                  | string          | 1.0.4.0    |
| DIAM_VPLMNId_MCCMNC                   | Значение MCC + MNC для `VPLMN-Id`.                                           | string          | 1.0.4.0    |
| DIAM_AID                              | Значение Diameter AVP: `Application-ID`.                                     | int             | 1.0.4.0    |
| DIAM_OpCode                           | Значение Diameter AVP: `Command-Code`.                                       | int             | 1.0.4.0    |
| DIAM_IsRequest                        | Флаг сообщения, являющегося запросом.                                        | bool            | 2023-08-23 |
| Tag                                   | Тег сообщения.                                                               | {string,string} | 2023-08-23 |
| [TCAP_Msg_Type](##tcap-message-type)  | Тип сообщения TCAP, Message Type.                                            | string          | 2023-08-23 |
| Velocity                              | Скорость перемещения.                                                        | int             | 1.0.9.0    |
| [VC_Status](#vc-status)               | Статус проверки скорости перемещения.                                        | int             | 1.0.9.0    |
| ATI_ERR_CODE                          | Cтатус запроса MAP-Any-Time-Interrogation.                                   | int             | 1.0.9.0    |
| ATI_VLR                               | Адрес узла VLR для запроса MAP-Any-Time-Interrogation.                       | string          | 1.0.9.0    |
| ORIG_REF                              | Значение TCAP Origination Reference.                                         | string          | 1.0.9.0    |
| DEST_REG                              | Значение Destination Register.                                               | string          | 1.0.9.0    |
| SRI4SM_ERR_CODE                       | Статус запроса MAP-Send-Routing-Info-Short-Message.                          | int             | 1.0.10.0   |
| SRI4SM_MSC                            | Адрес узла MSC для запроса MAP-Send-Routing-Info-Short-Message.              | string          | 1.0.10.0   |
| GTP_IMSI                              | Номер IMSI в GTP-запросах.                                                   | regex           |            |
| GTP_MSISDN                            | Номер MSISDN в GTP-запросах.                                                 | regex           |            |
| GTP_IMEI                              | Номер IMEI в GTP-запросах.                                                   | regex           |            |
| GTP_APN                               | Имя точки доступа, <abbr title="Access Point Name">APN</abbr>.               | string          |            |
| GTP_PLMN                              | Идентификатор PLMN.                                                          | int             |            |
| GTP_RAT_Type                          | Код типа <abbr title="Radio Access Technology">RAT</abbr>.                   | int             |            |
| GTP_Interface_Type                    | Код типа интерфейса.                                                         | int             |            |
| [GTP_Message_Type](#gtp-message-type) | Код типа запроса GTP.                                                        | int             |            |
| GTP_Version                           | Номер версии протокола GTP.                                                  | int             |            |
| Current network area                  | Текущая сетевая зона абонента.                                               | int             | 1.0.14.17  |

#### Статус VelocityCheck {#vc-status}

| Код | Сообщение                             | Описание                                                                                               |
|-----|---------------------------------------|--------------------------------------------------------------------------------------------------------|
| 0   | VC_STATUS_OK                          | Запрос успешен                                                                                         |
| 1   | VC_STATUS_NEI                         | Перемещение между соседями                                                                             |
| 2   | VC_STATUS_NO_LOC_ATI_FAILED           | Текущее местоположения не найдено в базе, запрос MAP-Any-Time-Interrogation не успешен                 |
| 3   | VC_STATUS_NO_LOC_ATI_NOT_ALLOWED      | Текущее местоположения не найдено в базе, формирование запроса MAP-Any-Time-Interrogation не разрешено |
| 4   | VC_STATUS_LOC_EXPIRED_ATI_NOT_ALLOWED | Время жизни сообщения истекло, формирование запроса MAP-Any-Time-Interrogation не разрешено            |
| 5   | VC_STATUS_LOC_EXPIRED_ATI_FAILED      | Время жизни сообщения истекло, запрос MAP-Any-Time-Interrogation не успешен                            |
| 6   | VC_STATUS_NO_LOC                      | Не удалось определить текущее местоположение: нет совпадений с базой Location                          |
| 7   | VC_STATUS_ATI_FAILED                  | Запрос MAP-Any-Time-Interrogation не успешен                                                           |
| 8   | VC_STATUS_AGE_0                       | Значение `AgeOfLocation` равно 0                                                                       |
| 9   | VC_STATUS_FAILURE                     | Системная ошибка                                                                                       |

#### Типы сообщений TCAP {#tcap-message-type}

Подробную информацию см. [ITU-T Q.771](https://www.itu.int/rec/dologin_pub.asp?lang=s&id=T-REC-Q.771-199706-I!!PDF-E&type=items).

| Код  | Тип            | Комментарий      |
|------|----------------|------------------|
| 0x01 | TC-BEGIN       |                  |
| 0x02 | TC-CONTINUE    |                  |
| 0x03 | TC-END         |                  |
| 0x04 | TC-UNI         |                  |
| 0x05 | TC-U-ABORT     |                  |
| 0x06 | TC-P-ABORT     | только индикация |
| 0x0C | TC-RESULT-L    |                  |
| 0x0D | TC-RESULT-NL   |                  |
| 0x0E | TC-U-ERROR     |                  |
| 0x0F | TC-L-CANCEL    | только индикация |
| 0x12 | TC-U-REJECT    |                  |
| 0x13 | TC-L-REJECT    | только индикация |
| 0x14 | TC-R-REJECT    | только индикация |
| 0x15 | TC-INVOKE      |                  |
| 0x16 | TC-U-CANCEL    | только запрос    |
| 0x17 | TC-NOTICE      | только индикация |
| 0x19 | TC-RESET-TIMER | только запрос    |


#### Типы сообщений GTP {#gtp-message-type}

Сообщения GTPv1, см. [3GPP TS 29.060](https://www.etsi.org/deliver/etsi_ts/129000_129099/129060/17.04.00_60/ts_129060v170400p.pdf).

| Код | Сообщение                                |
|-----|------------------------------------------|
| 1   | Echo Request                             |
| 2   | Echo Response                            |
| 3   | Version Not Supported                    |
| 4   | Node Alive Request                       |
| 5   | Node Alive Response                      |
| 6   | Redirection Request                      |
| 7   | Redirection Response                     |
| 16  | Create PDP Context Request               |
| 17  | Create PDP Context Response              |
| 18  | Update PDP Context Request               |
| 19  | Update PDP Context Response              |
| 20  | Delete PDP Context Request               |
| 21  | Delete PDP Context Response              |
| 22  | Initiate PDP Context Activation Request  |
| 23  | Initiate PDP Context Activation Response |
| 26  | Error Indication                         |
| 27  | PDU Notification Request                 |
| 28  | PDU Notification Response                |
| 29  | PDU Notification Reject Request          |
| 30  | PDU Notification Reject Response         |
| 31  | Supported Extensions Header Notification |
| 32  | Send Routing for GPRS Request            |
| 33  | Send Routing for GPRS Response           |
| 34  | Failure Report Request                   |
| 35  | Failure Report Response                  |
| 36  | Note MS Present Request                  |
| 37  | Note MS Present Response                 |
| 38  | Identification Request                   |
| 39  | Identification Response                  |
| 50  | SGSN Context Request                     |
| 51  | SGSN Context Response                    |
| 52  | SGSN Context Acknowledge                 |
| 53  | Forward Relocation Request               |
| 54  | Forward Relocation Response              |
| 55  | Forward Relocation Complete              |
| 56  | Relocation Cancel Request                |
| 57  | Relocation Cancel Response               |
| 58  | Forward SRNS Context                     |
| 59  | Forward Relocation Complete Acknowledge  |
| 60  | Forward SRNS Context Acknowledge         |
| 61  | UE Registration Request                  |
| 62  | UE Registration Response                 |
| 70  | RAN Information Relay                    |
| 96  | MBMS Notification Request                |
| 97  | MBMS Notification Response               |
| 98  | MBMS Notification Reject Request         |
| 99  | MBMS Notification Reject Response        |
| 100 | Create MBMS Notification Request         |
| 101 | Create MBMS Notification Response        |
| 102 | Update MBMS Notification Request         |
| 103 | Update MBMS Notification Response        |
| 104 | Delete MBMS Notification Request         |
| 105 | Delete MBMS Notification Response        |
| 112 | MBMS Registration Request                |
| 113 | MBMS Registration Response               |
| 114 | MBMS De-Registration Request             |
| 115 | MBMS De-Registration Response            |
| 116 | MBMS Session Start Request               |
| 117 | MBMS Session Start Response              |
| 118 | MBMS Session Stop Request                |
| 119 | MBMS Session Stop Response               |
| 120 | MBMS Session Update Request              |
| 121 | MBMS Session Update Response             |
| 128 | MS Info Change Request                   |
| 129 | MS Info Change Response                  |
| 240 | Data Record Transfer Request             |
| 241 | Data Record Transfer Response            |
| 254 | End Marker                               |
| 255 | G-PDU                                    |

Сообщения GTPv2, см. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.07.00_60/ts_129274v170700p.pdf).

| Код | Сообщение                            |
|-----|--------------------------------------|
| 1   | Echo Request                         |
| 2   | Echo Response                        |
| 3   | Version Not Supported                |
| 32  | Create Session Request               |
| 33  | Create Session Response              |
| 34  | Modify Bearer Request                |
| 35  | Modify Bearer Response               |
| 36  | Delete Session Request               |
| 37  | Delete Session Response              |
| 38  | Change Notification Request          |
| 39  | Change Notification Response         |
| 40  | Remote UE Report Notifications       |
| 41  | Remote UE Report Acknowledge         |
| 64  | Modify Bearer Command                |
| 65  | Modify Bearer Failure                |
| 66  | Delete Bearer Command                |
| 67  | Delete Bearer Failure                |
| 68  | Bearer Resource Command              |
| 69  | Bearer Resource Failure              |
| 70  | Downlink Data Notification Failure   |
| 71  | Trace Session Activation             |
| 72  | Trace Session Deactivation           |
| 73  | Stop Paging Indication               |
| 95  | Create Bearer Request                |
| 96  | Create Bearer Response               |
| 97  | Update Bearer Request                |
| 98  | Update Bearer Response               |
| 99  | Delete Bearer Request                |
| 100 | Delete Bearer Response               |
| 101 | Delete PDN Request                   |
| 102 | Delete PDN Response                  |
| 103 | PGW Downlink Notification            |
| 104 | PGW Downlink Acknowledge             |
| 128 | Identification Request               |
| 129 | Identification Response              |
| 130 | Context Request                      |
| 131 | Context Response                     |
| 132 | Context Acknowledge                  |
| 133 | Forward Relocation Request           |
| 134 | Forward Relocation Response          |
| 135 | Forward Relocation Notification      |
| 136 | Forward Relocation Acknowledge       |
| 137 | Forward Access Notification          |
| 138 | Forward Access Acknowledge           |
| 139 | Relocation Cancel Request            |
| 140 | Relocation Cancel Response           |
| 141 | Configuration Transfer Tunnel        |
| 149 | Detach Notification                  |
| 150 | Detach Acknowledge                   |
| 151 | CS Paging                            |
| 152 | RAN Information Relay                |
| 153 | Alert MME Notification               |
| 154 | Alert MME Acknowledge                |
| 155 | UE Activity Notification             |
| 156 | UE Activity Acknowledge              |
| 157 | ISR Status                           |
| 160 | Create Forwarding Request            |
| 161 | Create Forwarding Response           |
| 162 | Suspend Notification                 |
| 163 | Suspend Acknowledge                  |
| 164 | Resume Notification                  |
| 165 | Resume Acknowledge                   |
| 166 | Create Indirect Data Tunnel Request  |
| 167 | Create Indirect Data Tunnel Response |
| 168 | Delete Indirect Data Tunnel Request  |
| 169 | Delete Indirect Data Tunnel Response |
| 170 | Release Access Bearers Request       |
| 171 | Release Access Bearers Response      |
| 176 | Downlink Data Notification           |
| 177 | Downlink Data Acknowledge            |
| 179 | PGW Restart Notification             |
| 180 | PGW Restart Acknowledge              |
| 200 | Update PDN Connection Request        |
| 201 | Update PDN Connection Response       |
| 211 | Modify Access Bearers Request        |
| 212 | Modify Access Bearers Response       |
| 231 | MMBS Session Start Request           |
| 232 | MMBS Session Start Response          |
| 233 | MMBS Session Update Request          |
| 234 | MMBS Session Update Response         |
| 235 | MMBS Session Stop Request            |
| 236 | MMBS Session Stop Response           |