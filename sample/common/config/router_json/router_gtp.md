---
title: "RouterGTP"
description: "Параметры маршрутизатора RouterGTP"
weight: 30
type: docs
---

**RouterGTP** маршрутизирует сообщения по адресу вызывающего и вызываемого абонента.

### Описание параметров ###

| Параметр                                 | Описание                                                                                                                                  | Тип        | O/M | P/R | Версия |
|------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|-----|--------|
| ID                                       | Уникальный идентификатор узла маршрутизации.                                                                                              | int        | O   | R   |        |
| Name                                     | Уникальное имя узла маршрутизации.                                                                                                        | string     | M   | R   |        |
| Type                                     | Тип узла маршрутизации. Должно быть `RouterGTP`.                                                                                          | string     | M   | R   |        |
| Routing                                  | Параметры переадресации.                                                                                                                  | \[object\] | M   | R   |        |
| *{*                                      |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Key                          | Главный ключ для задания адресации. Формат:<br>`"Key": { "v1": {}, "v2": {} }`.                                                           | object     | O   | R   |        |
| &nbsp;&nbsp;*{*                          |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[v1](#gtp_codes) | Перечень кодов сообщений GTPv1.<br>Формат:<br>`"v1": { "MsgTypeList: [] }`.                                                               | \[int\]    | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[v2](#gtp_codes) | Перечень кодов сообщений GTPv2.<br>Формат:<br>`"v2": { "MsgTypeList: [] }`.                                                               | \[int\]    | O   | R   |        |
| &nbsp;&nbsp;*}*                          |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Route                        | Параметры последующей маршрутизации при совпадении ключа. Формат:<br>`"Route": { "GOTO": "<goto>" }`.                                     | object     | O   | R   |        |
| &nbsp;&nbsp;*{*                          |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;GOTO             | Название узла, куда перенаправляется запрос.<br>**Примечание.** Должно совпадать с именем существующего правила / маршрутизатора / "OUT". | string     | M   | R   |        |
| &nbsp;&nbsp;*}*                          |                                                                                                                                           |            |     |     |        |
| *}*                                      |                                                                                                                                           |            |     |     |        |

#### Пример ####

```json
{
  "Name": "GTP",
  "Type": "RouterGTP",
  "Routing": [
    {
      "Key": {
        "v1": { "MsgTypeList": [ 3, 2, 16, 17 ] },
        "v2": { "MsgTypeList": [ 2 ] }
      },
      "Route": { "GOTO": "Main" }
    }
  ]
}
```

#### Коды сообщений протокола GTP {#gtp_codes}

Подробную информацию см. [3GPP TS 29.060](https://www.etsi.org/deliver/etsi_ts/129000_129099/129060/17.04.00_60/ts_129060v170400p.pdf) и
[3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.09.00_60/ts_129274v170900p.pdf).

* Сообщения GTPv1:
  * 1 — Echo Request;
  * 2 — Echo Response;
  * 3 — Version Not Supported;
  * 4 — Node Alive Request;
  * 5 — Node Alive Response;
  * 6 — Redirection Request;
  * 7 — Redirection Response;
  * 16 — Create PDP Context Request;
  * 17 — Create PDP Context Response;
  * 18 — Update PDP Context Request;
  * 19 — Update PDP Context Response;
  * 20 — Delete PDP Context Request;
  * 21 — Delete PDP Context Response;
  * 22 — Initiate PDP Context Activation Request;
  * 23 — Initiate PDP Context Activation Response;
  * 26 — Error Indication;
  * 27 — PDU Notification Request;
  * 28 — PDU Notification Response;
  * 29 — PDU Notification Reject Request;
  * 30 — PDU Notification Reject Response;
  * 31 — Supported Extensions Header Notification;
  * 32 — Send Routing for GPRS Request;
  * 33 — Send Routing for GPRS Response;
  * 34 — Failure Report Request;
  * 35 — Failure Report Response;
  * 36 — Note MS Present Request;
  * 37 — Note MS Present Response;
  * 38 — Identification Request;
  * 39 — Identification Response;
  * 50 — SGSN Context Request;
  * 51 — SGSN Context Response;
  * 52 — SGSN Context Acknowledge;
  * 53 — Forward Relocation Request;
  * 54 — Forward Relocation Response;
  * 55 — Forward Relocation Complete;
  * 56 — Relocation Cancel Request;
  * 57 — Relocation Cancel Response;
  * 58 — Forward SRNS Context;
  * 59 — Forward Relocation Complete Acknowledge;
  * 60 — Forward SRNS Context Acknowledge;
  * 61 — UE Registration Request;
  * 62 — UE Registration Response;
  * 70 — RAN Information Relay;
  * 96 — MBMS Notification Request;
  * 97 — MBMS Notification Response;
  * 98 — MBMS Notification Reject Request;
  * 99 — MBMS Notification Reject Response;
  * 100 — Create MBMS Notification Request;
  * 101 — Create MBMS Notification Response;
  * 102 — Update MBMS Notification Request;
  * 103 — Update MBMS Notification Response;
  * 104 — Delete MBMS Notification Request;
  * 105 — Delete MBMS Notification Response;
  * 112 — MBMS Registration Request;
  * 113 — MBMS Registration Response;
  * 114 — MBMS De-Registration Request;
  * 115 — MBMS De-Registration Response;
  * 116 — MBMS Session Start Request;
  * 117 — MBMS Session Start Response;
  * 118 — MBMS Session Stop Request;
  * 119 — MBMS Session Stop Response;
  * 120 — MBMS Session Update Request;
  * 121 — MBMS Session Update Response;
  * 128 — MS Info Change Request;
  * 129 — MS Info Change Response;
  * 240 — Data Record Transfer Request;
  * 241 — Data Record Transfer Response;
  * 254 — End Marker;
  * 255 — G-PDU;
* Сообщения GTPv2:
  * 1 — Echo Request;
  * 2 — Echo Response;
  * 3 — Version Not Supported;
  * 32 — Create Session Request;
  * 33 — Create Session Response;
  * 34 — Modify Bearer Request;
  * 35 — Modify Bearer Response;
  * 36 — Delete Session Request;
  * 37 — Delete Session Response;
  * 38 — Change Notification Request;
  * 39 — Change Notification Response;
  * 40 — Remote UE Report Notifications;
  * 41 — Remote UE Report Acknowledge;
  * 64 — Modify Bearer Command;
  * 65 — Modify Bearer Failure;
  * 66 — Delete Bearer Command;
  * 67 — Delete Bearer Failure;
  * 68 — Bearer Resource Command;
  * 69 — Bearer Resource Failure;
  * 70 — Downlink Data Notification Failure;
  * 71 — Trace Session Activation;
  * 72 — Trace Session Deactivation;
  * 73 — Stop Paging Indication;
  * 95 — Create Bearer Request;
  * 96 — Create Bearer Response;
  * 97 — Update Bearer Request;
  * 98 — Update Bearer Response;
  * 99 — Delete Bearer Request;
  * 100 — Delete Bearer Response;
  * 101 — Delete PDN Request;
  * 102 — Delete PDN Response;
  * 103 — PGW Downlink Notification;
  * 104 — PGW Downlink Acknowledge;
  * 128 — Identification Request;
  * 129 — Identification Response;
  * 130 — Context Request;
  * 131 — Context Response;
  * 132 — Context Acknowledge;
  * 133 — Forward Relocation Request;
  * 134 — Forward Relocation Response;
  * 135 — Forward Relocation Notification;
  * 136 — Forward Relocation Acknowledge;
  * 137 — Forward Access Notification;
  * 138 — Forward Access Acknowledge;
  * 139 — Relocation Cancel Request;
  * 140 — Relocation Cancel Response;
  * 141 — Configuration Transfer Tunnel;
  * 149 — Detach Notification;
  * 150 — Detach Acknowledge;
  * 151 — CS Paging;
  * 152 — RAN Information Relay;
  * 153 — Alert MME Notification;
  * 154 — Alert MME Acknowledge;
  * 155 — UE Activity Notification;
  * 156 — UE Activity Acknowledge;
  * 157 — ISR Status;
  * 160 — Create Forwarding Request;
  * 161 — Create Forwarding Response;
  * 162 — Suspend Notification;
  * 163 — Suspend Acknowledge;
  * 164 — Resume Notification;
  * 165 — Resume Acknowledge;
  * 166 — Create Indirect Data Tunnel Request;
  * 167 — Create Indirect Data Tunnel Response;
  * 168 — Delete Indirect Data Tunnel Request;
  * 169 — Delete Indirect Data Tunnel Response;
  * 170 — Release Access Bearers Request;
  * 171 — Release Access Bearers Response;
  * 176 — Downlink Data Notification;
  * 177 — Downlink Data Acknowledge;
  * 179 — PGW Restart Notification;
  * 180 — PGW Restart Acknowledge;
  * 200 — Update PDN Connection Request;
  * 201 — Update PDN Connection Response;
  * 211 — Modify Access Bearers Request;
  * 212 — Modify Access Bearers Response;
  * 231 — MMBS Session Start Request;
  * 232 — MMBS Session Start Response;
  * 233 — MMBS Session Update Request;
  * 234 — MMBS Session Update Response;
  * 235 — MMBS Session Stop Request;
  * 236 — MMBS Session Stop Response;
