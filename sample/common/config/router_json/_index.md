---
title: "router.json"
description: "Параметры правил взаимодействия с сообщениями"
weight: 30
type: docs
---

В файле задаются настройки цепочек правил обработки сообщений.

Ключ для перегрузки - **reload RULES**.

### Описание параметров ###

| Параметр                  | Описание                                                                                                | Тип        | O/M | P/R | Версия |
|---------------------------|---------------------------------------------------------------------------------------------------------|------------|-----|-----|--------|
| Version                   | Актуальная версия всего конфигурационного файла.                                                        | int        | M   | R   |        |
| Lists                     | Параметры доступных версий файла. Формат:<br>`{ "ID": "<id>", "Version": <version> }`.                  | \[object\] | M   | R   |        |
| *{*                       |                                                                                                         |            |     |     |        |
| &nbsp;&nbsp;ID            | Идентификатор версии файла.                                                                             | string     | M   | R   |        |
| &nbsp;&nbsp;Version       | Номер версии файла.                                                                                     | int        | M   | R   |        |
| *}*                       |                                                                                                         |            |     |     |        |
| Routing                   | Параметры маршрутизации. Формат:<br>`{ "ID": <id>, "Name": "<name>", "Type": "<type>", "Routing": [] }` | \[object\] | O   | R   |        |
| *{*                       |                                                                                                         |            |     |     |        |
| &nbsp;&nbsp;ID            | Уникальный идентификатор узла маршрутизации.                                                            | int        | O   | R   |        |
| &nbsp;&nbsp;Name          | Уникальное имя узла маршрутизации.                                                                      | string     | M   | R   |        |
| &nbsp;&nbsp;[Type](#type) | Тип узла маршрутизации.                                                                                 | string     | M   | R   |        |
| &nbsp;&nbsp;Routing       | Параметры переадресации.                                                                                | \[object\] | M   | R   |        |
| *}*                       |                                                                                                         |            |     |     |        |

**Примечание.** Точка входа в цепочку правил — правило с именем **Main**.<br>
В файле router.json обязательно должно быть одно и только одно правило с именем Main.<br>
С него начинается обработка сообщения.

### Узлы маршрутизации {#type}

* **RouterProtocol** — маршрутизация по типу протокола;
* **RouterSCCP** — маршрутизация по адресам CdPA, CgPA;
* **RouterTCAP** — маршрутизация по OpCode и ACN;
* **RouterDIAM** — маршрутизация по OpCode и Application–Id;
* **RouterGTP** — маршрутизация по Message Type;
* **Rule** — правило маршрутизации.

#### Пример ####

```json
[
  {
    "Name": "Main",
    "Type": "RouterProtocol",
    "Routing": [
      {
        "Key": "SS7",
        "Route": { "GOTO": "OUTbound" }
      }
    ]
  },
  {
    "Name": "SS_messages",
    "Type": "Rule",
    "Key": [
      {
        "Field": "GT_A_Address",
        "Operator": "~",
        "Value": "7922.*",
        "Type": "String",
        "Condition": "AND"
      }
    ],
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "ATI",
    "Type": "Rule",
    "Actions": [
      {
        "Name": "ReturnError",
        "Data": { "ErrorCode": 34 }
      }
    ],
    "Next": "OUT"
  },
  {
    "Name": "any",
    "Type": "Rule",
    "Actions": [
      { "Name": "SendAlarm" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "UL",
    "Type": "Rule",
    "Actions": [
      {
        "Name": "VelocityCheck",
        "Data": {
          "AtiAllowed": 0,
          "EnabledPaging": 0,
          "ValidityPeriod": 0
        }
      }
    ],
    "Next": "UL_VC_OK"
  },
  {
    "Name": "OUTbound",
    "Type": "Rule",
    "Key": [
      {
        "Field": "GT_A_Address",
        "Operator": "~",
        "Value": "79.*",
        "Type": "String",
        "Condition": "AND"
      },
      {
        "Field": "GT_A_Address",
        "Operator": "==",
        "Value": "89*",
        "Type": "String",
        "Condition": "OR"
      }
    ],
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "INbound"
  },
  {
    "Name": "INbound",
    "Type": "RouterTCAP",
    "Routing": [
      {
        "Key": {
          "AppContextName": "04001032.|0400101.|04001010.",
          "OpCodeMask": { "variable": [ "UpdateLocation" ] },
          "AppContextType": "MAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "UL"
        }
      },
      {
        "Key": {
          "AppContextName": "04001018.",
          "OpCodeMask": { "value": "10|11|12|13|14" },
          "AppContextType": "MAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "SS_messages"
        }
      },
      {
        "Key": {
          "AppContextName": "0400121.|01001050.",
          "OpCodeMask": { "value": "0|24|36|44" },
          "AppContextType": "CAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "CAMEL"
        }
      },
      {
        "Key": {
          "AppContextName": "04001029.",
          "OpCodeMask": { "value": "71" },
          "AppContextType": "MAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "ATI"
        }
      },
      {
        "Key": {
          "AppContextName": "04001025.",
          "OpCodeMask": { "variable": [ "MT_ForwardSM" ] },
          "AppContextType": "MAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "MT_SMS"
        }
      },
      {
        "Key": {
          "AppContextName": "04001014.",
          "OpCodeMask": { "value": "56" },
          "AppContextType": "MAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "SAI"
        }
      },
      {
        "Key": {
          "AppContextName": "04001020.",
          "OpCodeMask": { "variable": [ "SendRoutingInfoForSM" ] },
          "AppContextType": "MAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "SRI_FOR_SM"
        }
      },
      {
        "Key": {
          "AppContextName": "0400105.",
          "OpCodeMask": { "value": "22" },
          "AppContextType": "MAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "SRI"
        }
      },
      {
        "Key": {
          "AppContextName": "04001019.",
          "OpCodeMask": { "value": "59" },
          "AppContextType": "MAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "USSD"
        }
      },
      {
        "Key": { "AppContextName": "04001021." },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "MO_FSM"
        }
      },
      {
        "Key": {
          "OpCodeMask": { "value": ".(0,22)" },
          "AppContextType": "MAP"
        },
        "Route": {
          "GT_A_set": { "Address": {} },
          "GT_B_set": { "Address": {} },
          "GOTO": "any"
        }
      }
    ]
  },
  {
    "Name": "CAMEL",
    "Type": "Rule",
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "MT_SMS",
    "Type": "Rule",
    "Key": [
      {
        "Field": "GT_B_Address",
        "Operator": "==",
        "Value": "79958964712",
        "Type": "String",
        "Condition": "AND"
      }
    ],
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "SAI",
    "Type": "Rule",
    "Key": [
      {
        "Field": "MAP_IMSI",
        "Operator": "==",
        "Value": "MAP_IMSI",
        "Type": "Field",
        "Option": "Prefix",
        "SizeOfPrefix": 25062,
        "Condition": "AND"
      }
    ],
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "SRI",
    "Type": "Rule",
    "Key": [
      {
        "Field": "GT_A_Address",
        "Operator": "!~",
        "Value": "79.*",
        "Type": "String",
        "Condition": "AND"
      }
    ],
    "Actions": [
      { "Name": "Reject" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "SRI_FOR_SM",
    "Type": "Rule",
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "USSD",
    "Type": "Rule",
    "Key": [
      {
        "Field": "MAP_MSISDN",
        "Operator": "==",
        "Value": "989999320694",
        "Type": "String",
        "Condition": "AND"
      }
    ],
    "Actions": [
      {
        "Name": "ReturnError",
        "Data": { "ErrorCode": 34 }
      }
    ],
    "Next": "USSD_OK"
  },
  {
    "Name": "UL_VC_Drop",
    "Type": "Rule",
    "Actions": [
      { "Name": "SilentDrop" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "UL_VC_OK",
    "Type": "Rule",
    "Key": [
      {
        "Field": "Velocity",
        "Operator": "<",
        "Value": "900",
        "Type": "Int",
        "Condition": "AND"
      },
      {
        "Field": "VC_Status",
        "Operator": "==",
        "Value": "0",
        "Type": "Int",
        "Condition": "AND"
      }
    ],
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "UL_VC_Drop"
  },
  {
    "Name": "USSD_OK",
    "Type": "Rule",
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "MO_FSM",
    "Type": "Rule",
    "Actions": [
      {
        "Name": "SendATI",
        "Data": {
          "EnabledPaging": 0,
          "ValidityPeriod": 0
        }
      }
    ],
    "Next": "MO_FSM_ATI_OK"
  },
  {
    "Name": "MO_FSM_ATI_OK",
    "Type": "Rule",
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "GTP-C",
    "Type": "RouterProtocol",
    "Routing": [
      {
        "Key": "DIAMETER",
        "Route": { "GOTO": "Rule_Diam" }
      }
    ]
  },
  {
    "Name": "Rule_Diam",
    "Type": "Rule",
    "Actions": [
      { "Name": "Transmit" }
    ],
    "Next": "OUT"
  },
  {
    "Name": "Diameter",
    "Type": "RouterDIAM",
    "Routing": [
      {
        "Key": { "OpCode": 317 },
        "Route": { "GOTO": "Rule_Diam" }
      }
    ]
  },
  {
    "Name": "GTP",
    "Type": "RouterGTP",
    "Routing": [
      {
        "Key": {
          "v1": { "MsgTypeList": [ 3, 2, 16, 17 ] },
          "v2": { "MsgTypeList": [ 2 ] }
        },
        "Route": { "GOTO": "Main" }
      }
    ]
  },
  {
    "Name": "Rule_category_2",
    "Type": "Rule",
    "Category": 2,
    "AttackType": "attack",
    "Key": [
      {
        "Field": "GTP_PLMN",
        "Operator": "in",
        "Value": "networkarea",
        "Type": "Network",
        "Condition": "AND"
      },
      {
        "Field": "GT_B_Address",
        "Operator": "==",
        "Value": "GT_A_Address",
        "Type": "Field",
        "Option": "Network",
        "Condition": "AND"
      },
      {
        "Field": "MAP_IMSI",
        "Operator": "!=",
        "Value": "GTP_IMSI",
        "Type": "Field",
        "Option": "Postfix",
        "Condition": "AND",
        "Start": 5
      }
    ],
    "Actions": [
      { "Name": "SetParam",
        "Data": {
          "paramName": { "variable": "GT_B_Address" },
          "valueType": "Field",
          "paramValueField": { "variable": "GT_A_Address" },
          "operator": "DelPrefix",
          "operatorValueString": { "VAL": "5" }
        }
      }
    ],
    "Next": "OUT"
  },
  {
    "Name": "Rule_set_value",
    "Type": "Rule",
    "Actions": [
      {
        "Name": "SetParam",
        "Data": {
          "paramName": { "variable": "MAP_MSISDN" },
          "valueType": "String",
          "paramValueString": "001010000000300"
        }
      }
    ],
    "Next": "OUT"
  },
  {
    "Name": "Rule_convert",
    "Type": "Rule",
    "Actions": [
      {
        "Name": "ConvertTo",
        "Data": {
          "Type": 1,
          "SMPP_Direction": 0
        }
      }
    ],
    "Next": "OUT"
  },
  {
    "Name": "Rule_modify_param",
    "Type": "Rule",
    "Actions": [
      {
        "Name": "SetParam",
        "Data": {
          "paramName": { "variable": "GT_B_Address" },
          "valueType": "Field",
          "paramValueField": { "variable": "GT_A_Address" }
        }
      }
    ],
    "Next": "OUT"
  }
]
```