---
title: "Спецификации и рекомендации"
description: "Перечень стандартов и рекомендаций, использованных при разработке продукта PROTEI MME"
weight: 100
type: docs
---

В разделе приведен перечень спецификаций и рекомендаций, использованных при разработке программного обеспечения узла PROTEI MME.

### 3GPP ###

| Номер спецификации                                                                                            | Название                                                                                                                                                                                                            |
|---------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.12.00_60/ts_123003v161200p.pdf) | Digital cellular telecommunications system (Phase 2+) (GSM);<br>Universal Mobile Telecommunications System (UMTS); LTE; 5G; Numbering, addressing and identification                                                |
| [3GPP TS 23.060](https://www.etsi.org/deliver/etsi_ts/123000_123099/123060/11.12.00_60/ts_123060v111200p.pdf) | Digital cellular telecommunications system (Phase 2+);<br>Universal Mobile Telecommunications System (UMTS);<br> General Packet Radio Service (GPRS); Service description; Stage 2                                  |
| [3GPP TS 23.216](https://www.etsi.org/deliver/etsi_ts/123200_123299/123216/17.01.00_60/ts_123216v170100p.pdf) | Digital cellular telecommunications system (Phase 2+) (GSM);<br>Universal Mobile Telecommunications System (UMTS); LTE;<br>Single Radio Voice Call Continuity (SRVCC); Stage 2                                      |
| [3GPP TS 23.272](https://www.etsi.org/deliver/etsi_ts/123200_123299/123272/17.00.00_60/ts_123272v170000p.pdf) | Digital cellular telecommunications system (Phase 2+) (GSM);<br>Universal Mobile Telecommunications System (UMTS);<br>LTE; Circuit Switched (CS) fallback in Evolved Packet System (EPS);<br>Stage 2                |
| [3GPP TS 23.401](https://www.etsi.org/deliver/etsi_ts/123400_123499/123401/16.14.00_60/ts_123401v161400p.pdf) | LTE; General Packet Radio Service (GPRS) enhancements for Evolved Universal Terrestrial Radio Access Network (E-UTRAN) access                                                                                       |
| [3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf) | Digital cellular telecommunications system (Phase 2+) (GSM);<br>Universal Mobile Telecommunications System (UMTS);<br>LTE; 5G; Mobile radio interface Layer 3 specification;<br>Core network protocols; Stage 3     |
| [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf) | Universal Mobile Telecommunications System (UMTS); LTE; 5G;<br>Non-Access-Stratum (NAS) protocol for Evolved Packet System (EPS); Stage 3                                                                           |
| [3GPP TS 25.413](https://www.etsi.org/deliver/etsi_ts/125400_125499/125413/17.00.00_60/ts_125413v170000p.pdf) | Universal Mobile Telecommunications System (UMTS);<br>UTRAN Iu interface Radio Access Network Application Part (RANAP) signalling                                                                                   |
| [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf) | Digital cellular telecommunications system (Phase 2+) (GSM);<br>Universal Mobile Telecommunications System (UMTS); LTE; 5G;<br>Mobile Application Part (MAP) specification                                          |
| [3GPP TS 29.060](https://www.etsi.org/deliver/etsi_ts/129000_129099/129060/14.04.00_60/ts_129060v140400p.pdf) | Digital cellular telecommunications system (Phase 2+);<br>Universal Mobile Telecommunications System (UMTS);<br> General Packet Radio Service (GPRS); GPRS Tunnelling Protocol (GTP) across the Gn and Gp interface |
| [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf) | Universal Mobile Telecommunications System (UMTS); LTE;<br>Mobility Management Entity (MME) - Visitor Location Register (VLR) SGs interface specification                                                           |
| [3GPP TS 29.128](https://www.etsi.org/deliver/etsi_ts/129100_129199/129128/17.01.00_60/ts_129128v170100p.pdf) | Universal Mobile Telecommunications System (UMTS); LTE;<br>Mobility Management Entity (MME) and Serving GPRS Support Node (SGSN) interfaces<br> for interworking with packet data networks and applications         |
| [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf) | Universal Mobile Telecommunications System (UMTS);<br>LTE; 5G; Evolved Packet System (EPS);<br>Mobility Management Entity (MME) and Serving GPRS Support Node (SGSN) related interfaces based on Diameter protocol  |
| [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/15.09.00_60/ts_129274v150900p.pdf) | Universal Mobile Telecommunications System (UMTS); LTE;<br>3GPP Evolved Packet System (EPS);<br> Evolved General Packet Radio Service (GPRS) Tunnelling Protocol for Control plane (GTPv2-C); Stage 3               |
| [3GPP TS 29.280](https://www.etsi.org/deliver/etsi_ts/129200_129299/129280/17.00.00_60/ts_129280v170000p.pdf) | Universal Mobile Telecommunications System (UMTS);<br>LTE; Evolved Packet System (EPS);<br>3GPP Sv interface (MME to MSC, and SGSN to MSC) for SRVCC                                                                |
| [3GPP TS 29.281](https://www.etsi.org/deliver/etsi_ts/129200_129299/129281/17.04.00_60/ts_129281v170400p.pdf) | Universal Mobile Telecommunications System (UMTS); LTE; 5G;<br>General Packet Radio System (GPRS) Tunnelling Protocol User Plane (GTPv1-U)                                                                          |
| [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf) | LTE; Evolved Universal Terrestrial Radio Access Network (E-UTRAN);<br>S1 Application Protocol (S1AP)                                                                                                                |
| [3GPP TS 43.051](https://www.etsi.org/deliver/etsi_ts/143000_143099/143051/17.00.00_60/ts_143051v170000p.pdf) | Digital cellular telecommunications system (Phase 2+) (GSM);<br>GSM/EDGE Overall description; Stage 2                                                                                                               |
| [3GPP TS 48.016](https://www.etsi.org/deliver/etsi_ts/148000_148099/148016/17.00.00_60/ts_148016v170000p.pdf) | Digital cellular telecommunications system (Phase 2+) (GSM);<br>General Packet Radio Service (GPRS);<br>Base Station System (BSS) - Serving GPRS Support Node (SGSN) interface;<br>Network service                  |
| [3GPP TS 48.018](https://www.etsi.org/deliver/etsi_ts/148000_148099/148018/17.00.00_60/ts_148018v170000p.pdf) | Digital cellular telecommunications system (Phase 2+) (GSM);<br>General Packet Radio Service (GPRS);<br>Base Station System (BSS) - Serving GPRS Support Node (SGSN);<br>BSS GPRS protocol (BSSGP)                  |

### RFC (Optional)

| Стандарт | Название                                                  |
|----------|-----------------------------------------------------------|
| RFC 5905 | Network Time Protocol, NTPv4                              |
| RFC 4251 | The Secure Shell (SSH) Protocol Architecture              |
| RFC 3588 | Diameter Base Protocol                                    |
| RFC 2866 | AAA Accounting                                            |
| RFC 2865 | Remote Authentication Dial In User Service (RADIUS)       |
| RFC 2818 | HTTP Over TLS                                             |
| RFC 2475 | An Architecture for Differentiated Service                |
| RFC 2474 | Definition of the Differentiated Service Field (DS Field) |
| RFC 2460 | Internet Protocol, version 6 (IPv6) specifications        |
| RFC 1945 | Hypertext Transfer Protocol -- HTTP/1.0                   |
| RFC 1881 | IPv6 Address Allocation Management                        |
| RFC 1771 | A Border Gateway Protocol (BGP-4)                         |
| RFC 1305 | Network Time Protocol (NTP) Version 3                     |
| RFC 1035 | Domain Names - Implementation and Specification           |
| RFC 1034 | Domain Names - Concepts and Facilities                    |
| RFC 793  | Transmission Control Protocol (TCP)                       |
| RFC 792  | Internet Control Message Protocol (ICMP)                  |
| RFC 791  | Internetwork Protocol (IP)                                |
| RFC 768  | User Datagram Protocol (UDP)                              |