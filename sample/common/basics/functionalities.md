---
title: "Функциональные возможности"
description: "Перечень функциональных возможностей узла PROTEI MME"
weight: 20
type: docs
---

Узел PROTEI MME выполняет следующие функции:

- Установление и отключение default и dedicated bearers (сквозных каналов)
- Поддержка до 11 bearers (сквозных каналов) на одно абонентское устройство;
- Шифрование и контроль целостности <abbr title="Non-Access Stratum">NAS</abbr>–сообщений;
- Управление процедурами подключения и отключения абонентских устройств к сети;
- Авторизация и аутентификация пользователей;
- Назначение временных идентификаторов для пользовательских устройств;
- Отслеживание и поиск абонентов;
- Выбор <abbr title="Packet Data Network Gateway">PGW</abbr>, <abbr title="Serving Gateway">SGW</abbr>, <abbr title="Serving GPRS Support Node">SGSN</abbr> или других <abbr title="Mobility Management Entity">MME</abbr> на основании локальных правил или DNS;
- Поддержка интерфейса S6a/S6d для взаимодействия с узлом HSS;
- Поддержка интерфейса S13 для взаимодействия с EIR;
- Поддержка протокола S1AP для взаимодействия с базовой станцией eNodeB;
- Поддержка механизма S1-Flex;
- Поддержка S1 и X2-Handover в LTE;
- Поддержка Inter-RAT Handover в 2G/3G c использованием интерфейсов Gn или S3;
- Поддержка работы в пуле MME c общей базой данных;
- Поддержка выделенных ядер сети в рамках концепции DECOR;
- Поддержка <abbr title="Circuit Switch Fallback">CSFB</abbr>;
- Поддержка <abbr title="Voice over LTE">VoLTE</abbr>;
- Поддержка <abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>;
- Поддержка <abbr title="Narrow Band Internet of Things">NB-IoT</abbr> радиосети LTE;
- Поддержка 5G <abbr title="Non Standalone">NSA</abbr>;
